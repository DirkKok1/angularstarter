﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api
{
    public class DateTimeWrapper
    {
        public static implicit operator DateTimeOffset(DateTimeWrapper dateTimeWrapper)
        {
            return DateTime.SpecifyKind(dateTimeWrapper.dateTime, DateTimeKind.Utc);
        }

        public static implicit operator DateTimeWrapper(DateTimeOffset dateTimeOffset)
        {
            return new DateTimeWrapper(dateTimeOffset.DateTime);
        }

        public static implicit operator DateTime(DateTimeWrapper dateTimeWrapper)
        {
            return dateTimeWrapper.dateTime;
        }

        public static implicit operator DateTimeWrapper(DateTime dateTime)
        {
            return new DateTimeWrapper(dateTime);
        }

        protected DateTimeWrapper(DateTime dateTime)
        {
            this.dateTime = dateTime;
        }

        private readonly DateTime dateTime;
    }
}