﻿using System;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.OData.Builder;
using System.Web.OData.Extensions;
using Api.Components.Claims;
using Api.Components.Orders;
using Api.Components.OrgNodes;
using Api.Components.Roles;
using Api.Components.Security;
using Microsoft.OData.Edm;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
            var allowedCors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(allowedCors);
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional }
                );
            config.MapODataServiceRoute("ODataRoute", "odata", GenerateEdmModel());

            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(
             new IsoDateTimeConverter());
        }

        private static IEdmModel GenerateEdmModel()
        {
            var builder = new ODataConventionModelBuilder();

            builder.EntitySet<User>("Users");
            builder.EntitySet<UserClaim>("UserClaims");
            builder.EntitySet<UserLogin>("UserLogins");
            builder.EntitySet<Role>("Roles");
            builder.EntitySet<Claim>("Claims");
            builder.EntitySet<OrgNode>("OrgNodes");
            builder.EntitySet<Order>("Orders");

            var order = builder.EntityType<Order>();
            order.Ignore(x => x.OrderDate);
            order.Property(x => x.OrderDateOffset).Name = "OrderDate";

            return builder.GetEdmModel();
        }
    }
}