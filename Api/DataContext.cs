﻿using System.Data.Entity;
using Api.Components.Claims;
using Api.Components.Orders;
using Api.Components.OrgNodes;
using Api.Components.Roles;
using Api.Components.Security;
using Api.Components.Security.RefreshTokens;

namespace Api
{
    public class DataContext : DbContext
    {
        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Claim> Claims { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserLogin> UserLogins { get; set; }
        public DbSet<UserClaim> UserClaims { get; set; }
        public DbSet<OrgNode> OrgNodes { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderLine> OrderLines { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>()
                .HasMany<Claim>(s => s.Claims)
                .WithMany(c => c.Roles)
                .Map(cs =>
                {
                    cs.MapLeftKey("RoleId");
                    cs.MapRightKey("ClaimId");
                    cs.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity<User>()
                .HasMany<Role>(s => s.Roles)
                .WithMany(c => c.Users)
                .Map(cs =>
                {
                    cs.MapLeftKey("UserId");
                    cs.MapRightKey("RoleId");
                    cs.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity<User>()
                .HasMany(c => c.UserClaims)
                .WithRequired(f => f.User)
                .HasForeignKey(x => x.UserId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<User>()
                .HasMany(c => c.UserLogins)
                .WithRequired(f => f.User)
                .HasForeignKey(x => x.UserId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<UserLogin>().HasKey(x => new {x.LoginProvider, x.ProviderKey, x.UserId});

            modelBuilder.Entity<OrgNode>()
                .HasMany<OrgNode>(s => s.Children)
                .WithOptional(x => x.Parent)
                .HasForeignKey(x => x.ParentId);

            modelBuilder.Entity<Order>()
                .HasMany(c => c.OrderLines)
                .WithRequired(f => f.Order)
                .HasForeignKey(x => x.OrderId)
                .WillCascadeOnDelete(true);

            base.OnModelCreating(modelBuilder);
        }
    }
}