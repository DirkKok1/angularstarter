﻿using System.Data.Entity;
using Api.Components.Security.RefreshTokens;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Api
{
    public class AuthContext : IdentityDbContext<IdentityUser>
    {
        public AuthContext()
            : base("AuthContext")
        {
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
    }
}