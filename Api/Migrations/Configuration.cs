using System.Collections.Generic;
using Api.Components.Authentication;
using Api.Components.Claims;
using Api.Components.Orders;
using Api.Components.OrgNodes;
using Api.Components.Roles;
using Api.Components.Security.RefreshTokens;

namespace Api.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Api.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(DataContext context)
        {

            #region Claims

            var claims = new List<Claim>
            {
                new Claim {ClaimType = "Route", ClaimValue = "/users", Name = "Users"},
                new Claim {ClaimType = "Route", ClaimValue = "/roles", Name = "Roles"},
                new Claim {ClaimType = "Route", ClaimValue = "/userAdd", Name = "Add Users"},
                new Claim {ClaimType = "Route", ClaimValue = "/userEdit", Name = "Edit Users"},
                new Claim {ClaimType = "Route", ClaimValue = "/roleEdit", Name = "Role Details"}
            };

            foreach (var newClaim in claims.Where(c => !context.Claims.Select(x => x.Name).Contains(c.Name)))
            {
                context.Claims.Add(newClaim);
            }

            context.SaveChanges();

            #endregion

            #region Default Client Type

            if (!context.Clients.Any(x => x.Id == "ngAuthApp"))
            {
                context.Clients.Add(new Client()
                {
                    Active = true,
                    AllowedOrigin = "*",
                    Id = "ngAuthApp",
                    Name = "AngularJS front-end Application",
                    ApplicationType = ApplicationTypes.JavaScript,
                    RefreshTokenLifeTime = 14400, //10 days
                    Secret = TokenHelper.GetHash("null") // javascript based app does not send secret - cannot be stored securely
                });
            }

            context.SaveChanges();

            #endregion

            #region Admin User 

            string adminUserId = "";
            if (!context.Users.Any(x => x.UserName == "admin"))
            {
                using (AuthRepository repo = new AuthRepository())
                {
                    var res = repo.RegisterUser(new CreateUserModel()
                    {
                        Password = "password",
                        ConfirmPassword = "password",
                        UserName = "admin"
                    });
                    adminUserId = res.Result.User.Id;
                }
            }

            context.SaveChanges();

            #endregion

            #region Admin Role

            var role = context.Roles.Include("Claims").FirstOrDefault(x => x.Name == "Administrator");

            if (role == null)
            {
                role = new Role() { Name = "Administrator" };
                context.Roles.Add(role);
            }

            context.SaveChanges();

            #endregion

            #region Add default claims

            foreach (var claim in claims.Where(c => !role.Claims.Select(x => x.Name).Contains(c.Name)))
            {
                role.Claims.Add(claim);
            }

            context.SaveChanges();

            #endregion

            #region Add default user to Admin role

            var user = context.Users.Where(x => x.Id == adminUserId).Include(y => y.Roles).FirstOrDefault();
            if (user != null && user.Roles.All(x => x.Id != role.Id))
            {
                user.Roles.Add(role);
                context.SaveChanges();
            }

            #endregion

            #region Org Nodes

            var rootNode = context.OrgNodes.FirstOrDefault(x => x.ParentId == null);
            if (rootNode == null)
            {
                rootNode = new OrgNode() {Name = "Root"};
                rootNode.Children.Add(new OrgNode() { Name = "Child 1"});
                rootNode.Children.Add(new OrgNode() { Name = "Child 2" });
                rootNode.Children[0].Children.Add(new OrgNode() {Name = "Child 1_1"});

                context.OrgNodes.Add(rootNode);
                context.SaveChanges();
            }

            #endregion

            #region Orders

            var order = context.Orders.FirstOrDefault();
            if (order == null)
            {
                order = new Order() {Name = "Test Order", OrderDate = DateTime.Now };
                order.OrderLines.Add(new OrderLine() { Description = "Item 1", Price = 10, Qty = 4, Position = 1 });
                order.OrderLines.Add(new OrderLine() { Description = "Item 2", Price = 5, Qty = 7, Position = 2});

                context.Orders.Add(order);
                context.SaveChanges();
            }

            #endregion


        }
    }
}
