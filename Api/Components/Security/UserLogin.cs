﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Components.Security
{
    [Table("AspNetUserLogins")]
    public class UserLogin
    {
        [Key]
        public string LoginProvider { get; set; }

        [Key]
        public string ProviderKey { get; set; }

        [Key]
        [ForeignKey("User")]
        public string UserId { get; set; }

        public virtual User User { get; set; }
    }
}