﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Components.Security
{
    [Table("AspNetUserClaims")]
    public class UserClaim
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }

        public virtual User User { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
    }
}