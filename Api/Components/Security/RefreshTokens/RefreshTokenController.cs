﻿using System.Web.Http;

namespace Api.Components.Security.RefreshTokens
{
    [Authorize]
    public class RefreshTokenController : ApiController
    {
        public IHttpActionResult CheckTokenValidity()
        {
            return Ok();
        }
    }
}