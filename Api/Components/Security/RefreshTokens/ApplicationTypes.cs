﻿namespace Api.Components.Security.RefreshTokens
{
    public enum ApplicationTypes
    {
        JavaScript = 0,
        NativeConfidential = 1
    }
}