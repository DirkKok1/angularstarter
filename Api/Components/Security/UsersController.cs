﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.OData;
using Api.Components.Roles;
using System;

namespace Api.Components.Security
{
    [Authorize]
    public class UsersController : ODataController
    {
        private readonly DataContext db = new DataContext();
        // GET: odata/Users
        [EnableQuery]
        public IQueryable<User> GetUsers()
        {
            return db.Users;
        }

        // GET: odata/Users(5)
        [EnableQuery]
        public SingleResult<User> GetUser([FromODataUri] string key)
        {
            var result = db.Users.Where(user => user.Id == key).Include(y => y.Roles);
            var op = SingleResult.Create(result);
            return op;
        }

        // PUT: odata/Users(5)
        public async Task<IHttpActionResult> Put([FromODataUri] string key, User newUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var existingUser = db.Users.Find(key);
            if (existingUser == null)
            {
                return NotFound();
            }

            db.Entry(existingUser).Collection(x => x.Roles).Load();

            var roleIds = newUser.Roles.Select(x => x.Id).ToList();
            newUser.Roles = new List<Role>();

            // Remove roles not in the list
            existingUser.Roles.RemoveAll(x => !roleIds.Contains(x.Id));

            // Find the ids of the roles we need to add
            var existingRoleIds = new HashSet<string>(existingUser.Roles.Select(x => x.Id));
            var roleIdsToAdd = roleIds.Where(x => !existingRoleIds.Contains(x));

            // Add the roles to the newUser
            foreach (var role in db.Roles.Where(x => roleIdsToAdd.Contains(x.Id)))
            {
                existingUser.Roles.Add(role);
            }

            // Overwrite the existing newUser values to the new newUser values
            db.Entry(existingUser).CurrentValues.SetValues(newUser);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(key))
                {
                    return NotFound();
                }
                throw;
            }

            return Updated(newUser);
        }

        // POST: odata/Users
        public async Task<IHttpActionResult> Post(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Get the list of role ids
            var newRoles = user.Roles.Select(x => x.Id);
            user.Roles = new List<Role>();
            // We need to attach the existing roles from the database and not from the client
            user.Roles.AddRange(db.Roles.Where(x => newRoles.Contains(x.Id)));

            db.Users.Add(user);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UserExists(user.Id))
                {
                    return Conflict();
                }
                throw;
            }

            return Created(user);
        }


        // DELETE: odata/Users(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] string key)
        {
            var user = await db.Users.FindAsync(key);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Roles(5)/Roles
        [EnableQuery]
        public IQueryable<Role> GetRoles([FromODataUri] string key)
        {
            return db.Users.Where(m => m.Id == key).SelectMany(m => m.Roles);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(string key)
        {
            return db.Users.Count(e => e.Id == key) > 0;
        }
    }
}