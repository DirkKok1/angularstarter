namespace Api.Components.Versioning
{
    public class VersionModel
    {
        /// <summary>
        ///     Gets or sets the API version.
        /// </summary>
        /// <value>The API version.</value>
        public string ApiVersion { get; set; }

        /// <summary>
        ///     Gets or sets the database version.
        /// </summary>
        /// <value>The database version.</value>
        public string DatabaseVersion { get; set; }
    }
}