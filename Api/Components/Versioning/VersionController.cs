﻿using System.Reflection;
using System.Web.Http;

namespace Api.Components.Versioning
{
    /// <summary>
    ///     API controller for version information.
    /// </summary>
    public class VersionController : ApiController
    {
        /// <summary>
        ///     Gets the version information of the API.
        /// </summary>
        /// <returns>The version information</returns>
        public IHttpActionResult GetVersion()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var apiVersion = assembly.GetName().Version;

            // TODO: Return database version schema
            var databaseVersion = "Unknown";

            return Ok(new VersionModel {ApiVersion = apiVersion.ToString(), DatabaseVersion = databaseVersion});
        }
    }
}