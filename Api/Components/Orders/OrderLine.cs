﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Api.Components.Orders
{
    [Table("OrderLine")]
    public class OrderLine
    {
        private Guid id = Guid.NewGuid();

        [Key]
        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Description { get; set; }

        public int Qty { get; set; }

        public double Price { get; set; }

        public int Position { get; set; }

        [ForeignKey("Order")]
        public Guid? OrderId { get; set; }

        public virtual Order Order { get; set; }
    }
}