﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Api.Components.OrgNodes;

namespace Api.Components.Orders
{
    [Table("Order")]
    public class Order
    {
        private Guid id = Guid.NewGuid();

        [Required]
        public string Name { get; set; }

        [Key]
        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }

        [NotMapped]
        public DateTimeOffset OrderDateOffset
        {
            get { return orderDate; }
            set { orderDate = value; }
        }

        private DateTimeWrapper orderDate;

        public DateTime OrderDate
        {
            get { return orderDate; }
            set { orderDate = value; }
        }
        

        public virtual List<OrderLine> OrderLines { get; set; } = new List<OrderLine>();
    }
}