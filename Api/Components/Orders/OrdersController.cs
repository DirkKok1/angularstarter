﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.OData;

namespace Api.Components.Orders
{
    [Authorize]
    public class OrdersController : ODataController
    {
        private readonly DataContext db = new DataContext();

        // GET: odata/Orders
        [EnableQuery]
        public IQueryable<Order> GetOrders()
        {
            return db.Orders;
        }

        // GET: odata/Orders(5)
        [EnableQuery]
        public SingleResult<Order> GetOrder([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.Orders.Where(order => order.Id == key).Include(x => x.OrderLines));
        }

        // PUT: odata/Orders(5)
        public async Task<IHttpActionResult> Put([FromODataUri] Guid key, Order newOrder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var existingOrder = db.Orders.Find(key);
            if (existingOrder == null)
            {
                return NotFound();
            }

            db.Entry(existingOrder).Collection(x => x.OrderLines).Load();

            // Update order line properties
            foreach (var orderLine in existingOrder.OrderLines)
            {
                var newOrderLine = newOrder.OrderLines.FirstOrDefault(x => x.Id == orderLine.Id);
                if (newOrderLine != null)
                {
                    db.Entry(orderLine).CurrentValues.SetValues(newOrderLine);
                }
            }

            var orderLineIds = newOrder.OrderLines.Select(x => x.Id).ToList();
            newOrder.OrderLines = new List<OrderLine>();

            // Remove orderLines not in the list
            existingOrder.OrderLines.RemoveAll(x => !orderLineIds.Contains(x.Id));


            // Find the ids of the orderLines we need to add
            var existingOrderLineIds = new HashSet<Guid>(existingOrder.OrderLines.Select(x => x.Id));
            var orderLineIdsToAdd = orderLineIds.Where(x => !existingOrderLineIds.Contains(x));

            // Add the orderLines to the newOrder
            foreach (var orderLine in db.OrderLines.Where(x => orderLineIdsToAdd.Contains(x.Id)))
            {
                existingOrder.OrderLines.Add(orderLine);
            }

            // Overwrite the existing newOrder values to the new newOrder values
            db.Entry(existingOrder).CurrentValues.SetValues(newOrder);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(key))
                {
                    return NotFound();
                }
                throw;
            }

            return Updated(newOrder);
        }

        // POST: odata/Orders
        public async Task<IHttpActionResult> Post(Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Get the list of orderLine ids
            var newOrderLines = order.OrderLines.Select(x => x.Id);
            order.OrderLines = new List<OrderLine>();
            // We need to attach the existing orderLines from the database and not from the client
            order.OrderLines.AddRange(db.OrderLines.Where(x => newOrderLines.Contains(x.Id)));

            db.Orders.Add(order);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (OrderExists(order.Id))
                {
                    return Conflict();
                }
                throw;
            }

            return Created(order);
        }

        // DELETE: odata/Orders(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] Guid key)
        {
            var order = await db.Orders.FindAsync(key);
            if (order == null)
            {
                return NotFound();
            }

            db.Orders.Remove(order);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Orders(5)/OrderLines
        [EnableQuery]
        public IQueryable<OrderLine> GetOrderLines([FromODataUri] Guid key)
        {
            return db.Orders.Where(m => m.Id == key).SelectMany(m => m.OrderLines);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrderExists(Guid key)
        {
            return db.Orders.Count(e => e.Id == key) > 0;
        }
    }
}