﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.OData;

namespace Api.Components.OrgNodes
{
    [Authorize]
    public class OrgNodesController : ODataController
    {
        private readonly DataContext db = new DataContext();

        // GET: odata/OrgNodes
        [EnableQuery]
        public IQueryable<OrgNodes.OrgNode> GetOrgNodes()
        {
            var results = GetODataQueryString(Request);

            if (results.ContainsKey("id"))
            {
                var parentId = new Guid(results["id"]);
                return db.OrgNodes.Where(x => x.ParentId == parentId);
            }

            return db.OrgNodes.Where(x => x.ParentId == null);
        }

        private static Dictionary<string, string> GetODataQueryString(HttpRequestMessage request)
        {
            return request.GetQueryNameValuePairs()
                .ToDictionary(kv => kv.Key, kv => kv.Value,
                    StringComparer.OrdinalIgnoreCase);
        }


        // GET: odata/OrgNodes(5)
        [EnableQuery]
        public SingleResult<OrgNodes.OrgNode> GetOrgNode([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.OrgNodes.Where(orgNode => orgNode.Id == key));
        }

        // PUT: odata/OrgNodes(5)
        public async Task<IHttpActionResult> Put([FromODataUri] Guid key, Delta<OrgNodes.OrgNode> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var orgNode = await db.OrgNodes.FindAsync(key);
            if (orgNode == null)
            {
                return NotFound();
            }

            patch.Put(orgNode);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrgNodeExists(key))
                {
                    return NotFound();
                }
                throw;
            }

            return Updated(orgNode);
        }

        // POST: odata/OrgNodes
        public async Task<IHttpActionResult> Post(OrgNodes.OrgNode orgNode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.OrgNodes.Add(orgNode);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (OrgNodeExists(orgNode.Id))
                {
                    return Conflict();
                }
                throw;
            }

            return Created(orgNode);
        }

        // PATCH: odata/OrgNodes(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] Guid key, Delta<OrgNodes.OrgNode> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var orgNode = await db.OrgNodes.FindAsync(key);
            if (orgNode == null)
            {
                return NotFound();
            }

            patch.Patch(orgNode);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrgNodeExists(key))
                {
                    return NotFound();
                }
                throw;
            }

            return Updated(orgNode);
        }

        // DELETE: odata/OrgNodes(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] Guid key)
        {
            var orgNode = await db.OrgNodes.FindAsync(key);
            if (orgNode == null)
            {
                return NotFound();
            }

            db.OrgNodes.Remove(orgNode);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/OrgNodes(5)/Children
        [EnableQuery]
        public IQueryable<OrgNodes.OrgNode> GetChildren([FromODataUri] Guid key)
        {
            return db.OrgNodes.Where(m => m.Id == key).SelectMany(m => m.Children);
        }

        // GET: odata/OrgNodes(5)/Parent
        [EnableQuery]
        public SingleResult<OrgNodes.OrgNode> GetParentOrgNode([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.OrgNodes.Where(m => m.Id == key).Select(m => m.Parent));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrgNodeExists(Guid key)
        {
            return db.OrgNodes.Count(e => e.Id == key) > 0;
        }
    }
}