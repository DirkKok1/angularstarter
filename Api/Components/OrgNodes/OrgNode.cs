﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Components.OrgNodes
{
    [Table("OrgNode")]
    public class OrgNode
    {
        private Guid id = Guid.NewGuid();

        [Required]
        public string Name { get; set; }

        [Key]
        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }

        [ForeignKey("Parent")]
        public Guid? ParentId { get; set; }

        public virtual OrgNode Parent { get; set; }

        public virtual List<OrgNode> Children { get; set; } = new List<OrgNode>();
    }
}