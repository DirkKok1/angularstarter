﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Api.Components.Claims;
using Api.Components.Security;

namespace Api.Components.Roles
{
    [Table("AspNetRoles")]
    public class Role
    {
        [Key]
        public string Id { get; set; } = Guid.NewGuid().ToString();

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        public string Name { get; set; }

        public virtual List<Claim> Claims { get; set; } = new List<Claim>();
        public bool CanAccessAllModules { get; set; } = true;
        public bool CanAccessAllData { get; set; } = true;
        public virtual List<User> Users { get; set; } = new List<User>();
    }
}