using System.Security.Cryptography;
using System.Text;

namespace Api.Components.Authentication
{
    public class TokenHelper
    {
        public static string GetHash(string source)
        {
            var crypt = new SHA256Managed();
            var hash = new StringBuilder();
            var crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(source), 0, Encoding.UTF8.GetByteCount(source));
            foreach (var bit in crypto)
            {
                hash.Append(bit.ToString("x2"));
            }
            return hash.ToString();
        }
    }
}