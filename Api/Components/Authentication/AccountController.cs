﻿using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;

namespace Api.Components.Authentication
{
    [RoutePrefix("api/Account")]
    [Authorize]
    public class AccountController : ApiController
    {
        private readonly AuthRepository repo;

        public AccountController()
        {
            repo = new AuthRepository();
        }

        // POST api/Account/ChangePassword
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangeUserPasswordModel changeUserPasswordModel)
        {
            var result = await repo.ChangePassword(changeUserPasswordModel);

            var errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }

        // POST api/Account/Register
        [Route("Register")]
        public async Task<IHttpActionResult> Register(CreateUserModel createUserModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await repo.RegisterUser(createUserModel);

            var errorResult = GetErrorResult(result.Result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok(result.User.Id);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                repo.Dispose();
            }

            base.Dispose(disposing);
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}