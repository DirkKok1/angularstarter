﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Components.Claims;
using Api.Components.Roles;
using Api.Components.Security.RefreshTokens;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Api.Components.Authentication
{
    public class AuthRepository : IDisposable
    {
        private readonly AuthContext authContext;
        private readonly DataContext db = new DataContext();
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<IdentityUser> userManager;

        public AuthRepository()
        {
            authContext = new AuthContext();
            userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(authContext));
            roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(authContext));
        }

        public void Dispose()
        {
            authContext.Dispose();
            userManager.Dispose();
        }

        public Client FindClient(string clientId)
        {
            var client = authContext.Clients.Find(clientId);
            return client;
        }

        public async Task<bool> AddRefreshToken(RefreshToken token)
        {
            var existingToken =
                authContext.RefreshTokens.Where(r => r.Subject == token.Subject && r.ClientId == token.ClientId)
                    .SingleOrDefault();

            if (existingToken != null)
            {
                var result = await RemoveRefreshToken(existingToken);
            }

            authContext.RefreshTokens.Add(token);

            return authContext.SaveChanges() > 0;
        }

        public async Task<bool> RemoveRefreshToken(string refreshTokenId)
        {
            var refreshToken = await authContext.RefreshTokens.FindAsync(refreshTokenId);

            if (refreshToken != null)
            {
                authContext.RefreshTokens.Remove(refreshToken);
                return await authContext.SaveChangesAsync() > 0;
            }
            return false;
        }

        public async Task<bool> RemoveRefreshToken(RefreshToken refreshToken)
        {
            authContext.RefreshTokens.Remove(refreshToken);
            return await authContext.SaveChangesAsync() > 0;
        }

        public async Task<RefreshToken> FindRefreshToken(string refreshTokenId)
        {
            var refreshToken = await authContext.RefreshTokens.FindAsync(refreshTokenId);

            return refreshToken;
        }

        public List<RefreshToken> GetAllRefreshTokens()
        {
            return authContext.RefreshTokens.ToList();
        }

        public async Task<IdentityResult> ChangePassword(ChangeUserPasswordModel changeUserPasswordModel)
        {
            var result =
                await
                    userManager.ChangePasswordAsync(changeUserPasswordModel.UserId, changeUserPasswordModel.CurrentPassword,
                        changeUserPasswordModel.NewPassword);

            return result;
        }

        public async Task<RegisterResult> RegisterUser(CreateUserModel createUserModel)
        {
            var user = new IdentityUser
            {
                UserName = createUserModel.UserName
            };
            
            var result = await userManager.CreateAsync(user, createUserModel.Password);
            if (result.Succeeded)
            {
                var rolesToAdd = db.Roles.Where(x => createUserModel.Roles.Contains(x.Id));
                var dbUser = db.Users.Find(user.Id);
                dbUser.Roles.AddRange(rolesToAdd);
                await db.SaveChangesAsync();
            }
            return new RegisterResult {Result = result, User = user};
        }

        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            var user = await userManager.FindAsync(userName, password);
            return user;
        }

        public Role FindRole(string id)
        {
            var role = db.Roles.FirstOrDefault(x => x.Id == id);
            return role;
        }

        public IQueryable<Claim> FindRoleClaims(string roleId)
        {
            return db.Claims.Where(x => x.Roles.Any(role => role.Id == roleId));
        }

        public IQueryable<Claim> GetAllClaims()
        {
            return db.Claims;
        }

        public class RegisterResult
        {
            public IdentityResult Result { get; set; }
            public IdentityUser User { get; set; }
        }
    }
}