﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.OData;
using Api.Components.Roles;

namespace Api.Components.Claims
{
    [Authorize]
    public class ClaimsLookUpController : ApiController
    {
        private readonly DataContext db = new DataContext();

        public IEnumerable<object> GetClaims(Guid userID)
        {
            var identity = User.Identity as ClaimsIdentity;
            var claims = new List<object>();
            if (identity == null)
            {
                return claims;
            }
            claims.AddRange(
                identity.Claims.Where(c => c.Type.Equals("Route", StringComparison.OrdinalIgnoreCase)).Select(claim =>
                    new
                    {
                        Route = claim.Value
                    }));
            return claims;
        }
    }


    [Authorize]
    public class ClaimsController : ODataController
    {
        private readonly DataContext db = new DataContext();
        // GET: odata/Claims
        [EnableQuery]
        public IQueryable<Claim> GetClaims()
        {
            return db.Claims;
        }

        // GET: odata/Claims(5)
        [EnableQuery]
        public SingleResult<Claim> GetClaim([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.Claims.Where(claim => claim.Id == key));
        }

        // PUT: odata/Claims(5)
        public async Task<IHttpActionResult> Put([FromODataUri] Guid key, Delta<Claim> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var claim = await db.Claims.FindAsync(key);
            if (claim == null)
            {
                return NotFound();
            }

            patch.Put(claim);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClaimExists(key))
                {
                    return NotFound();
                }
                throw;
            }

            return Updated(claim);
        }

        // POST: odata/Claims
        public async Task<IHttpActionResult> Post(Claim claim)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Claims.Add(claim);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ClaimExists(claim.Id))
                {
                    return Conflict();
                }
                throw;
            }

            return Created(claim);
        }

        // PATCH: odata/Claims(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] Guid key, Delta<Claim> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var claim = await db.Claims.FindAsync(key);
            if (claim == null)
            {
                return NotFound();
            }

            patch.Patch(claim);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClaimExists(key))
                {
                    return NotFound();
                }
                throw;
            }

            return Updated(claim);
        }

        // DELETE: odata/Claims(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] Guid key)
        {
            var claim = await db.Claims.FindAsync(key);
            if (claim == null)
            {
                return NotFound();
            }

            db.Claims.Remove(claim);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Claims(5)/Roles
        [EnableQuery]
        public IQueryable<Role> GetRoles([FromODataUri] Guid key)
        {
            return db.Claims.Where(m => m.Id == key).SelectMany(m => m.Roles);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ClaimExists(Guid key)
        {
            return db.Claims.Count(e => e.Id == key) > 0;
        }
    }
}