/// <reference path="../typings/angularjs/angular.d.ts" />
/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/linq/linq.d.ts" />
/// <reference path="../typings/moment/moment.d.ts" />

'use strict';

define(['shared/routeResolver'], function () {

  var app = angular.module('angularStarter', ['ngRoute', 'ngMessages', 'LocalStorageModule', 'routeResolverServices', 'ui.bootstrap', 'angular-loading-bar', 'kendo.directives', 'ngDialog', 'angularFileUpload', 'ngSanitize', 'ui.select']);

  app.config(['$routeProvider', 'routeResolverProvider', '$controllerProvider',
    '$compileProvider', '$filterProvider', '$provide', '$httpProvider', 'ngDialogProvider',

    function ($routeProvider, routeResolverProvider, $controllerProvider,
              $compileProvider, $filterProvider, $provide, $httpProvider, ngDialogProvider) {

      //Change default views and controllers directory using the following:
      //routeResolverProvider.routeConfig.setBaseDirectories('/app/views', '/app/controllers');

      $httpProvider.interceptors.push('authInterceptorService');

      ngDialogProvider.setDefaults({
        className: 'ngdialog-theme-default',
        plain: false,
        showClose: false,
        closeByDocument: true,
        closeByEscape: true,
        appendTo: false,
        preCloseCallback: function () {

        }
      });

      app.register =
      {
        controller: $controllerProvider.register,
        directive: $compileProvider.directive,
        filter: $filterProvider.register,
        factory: $provide.factory,
        service: $provide.service
      };

      //Define routes - controllers will be loaded dynamically
      var route = routeResolverProvider.route;

      $routeProvider
        //route.resolve() now accepts the convention to use (name of controller & view) as well as the
        //path where the controller or view lives in the controllers or views folder if it's in a sub folder.
        //For example, the controllers for customers live in controllers/customers and the views are in views/customers.
        //The controllers for orders live in controllers/orders and the views are in views/orders
        //The second parameter allows for putting related controllers/views into subfolders to better organize large projects
        //Thanks to Ton Yeung for the idea and contribution
        //.when('/customers', route.resolve('Customers', 'customers/', 'vm'))
        //.when('/customerorders/:customerId', route.resolve('CustomerOrders', 'customers/', 'vm'))
        //.when('/customeredit/:customerId', route.resolve('CustomerEdit', 'customers/', 'vm', true))
        //.when('/orders', route.resolve('Orders', 'orders/', 'vm'))
        .when('/treeListView', route.resolve('TreeListView', 'treeListView/', 'vm', true))
        .when('/treeView', route.resolve('TreeView', 'treeView/', 'vm', true))
        .when('/orgNodeEdit/:orgNodeId?', route.resolve('OrgNodeEdit', 'orgNodeEdit/', 'vm', true))
        .when('/orders', route.resolve('Orders', 'orders/', 'vm', true))
        .when('/orderEdit/:orderId?', route.resolve('OrderEdit', 'orderEdit/', 'vm', true))
        .when('/users', route.resolve('Users', 'users/', 'vm', true))
        .when('/userAdd', route.resolve('UserAdd', 'userAdd/', 'vm', true))
        .when('/userEdit/:userId', route.resolve('UserEdit', 'userEdit/', 'vm', true))
        .when('/roles', route.resolve('Roles', 'roles/', 'vm', true))
        .when('/roleEdit/:roleId?', route.resolve('RoleEdit', 'roleEdit/', 'vm', true))
        .when('/manageAccount', route.resolve('ManageAccount', 'manageAccount/', 'vm', true))
        .when('/changePassword', route.resolve('ChangePassword', 'changePassword/', 'vm', true))
        .when('/login', route.resolve('Login', 'login/', 'vm'))
        .when('/login/:redirect*?', route.resolve('Login', 'login/', 'vm'))
        .otherwise({redirectTo: '/login'});

    }]);

  app.run(['$rootScope', '$location', 'authService', 'securityService',
    function ($rootScope, $location, authService, securityService, $injector) {
      authService.fillAuthData();

      //securityService.initializeSecurity();

      //Client-side security. Server-side framework MUST add it's
      //own security as well since client-based security is easily hacked
      $rootScope.$on("$routeChangeStart", function (event, next, current) {

        if (next && next.$$route && next.$$route.secure) {
          if (!authService.authentication.isAuth) {
            $rootScope.$evalAsync(function () {
              authService.redirectToLogin();
            });
          }
        }
      });


      $(document).ajaxError(function (event, jqxhr, settings) {

        if (jqxhr.status == 401) {
          if (!settings.secondExec) {
            authService.refreshToken().then(function (response) {
              settings.secondExec = true;
              $.ajax(settings);
            });
          } else {
            authService.logOut();
            $location.path('/login');
          }
        }
      });


    }]);


  //TODO Move to diff module - security

  app.directive('accessClaim', ['$rootScope', 'securityService', function ($rootScope, securityService) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        var prevDisplay;
        prevDisplay = element.css('display');
        if (!securityService.authorizePath(attrs.accessClaim)) {
          return element.css('display', 'none');
        } else {
          element.css('display', 'block');
        }
        return $rootScope.$on('loginStatusChanged', function (event) {
          if (!securityService.authorizePath(attrs.accessClaim)) {
            return element.css('display', 'none');
          } else {
            element.css('display', 'block');
          }
        });
      }
    };
  }]);

  app.directive('accessClaimGroup', ['$rootScope', 'securityService', function ($rootScope, securityService) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        var prevDisplay;
        prevDisplay = element.css('display');

        var authElements = element[0].children[0].children;
        var allow = false;
        
        for (var i = 0; i < authElements.length; i++) {
          
          if (authElements[i].children[0].hasAttribute("access-claim"))
          {
            allow = securityService.authorizePath(authElements[i].children[0].hash.substring(1, authElements[i].children[0].hash.length));
          }
          else
          {
            allow = true;
          }
          
          if (allow) 
          {
            return element.css('display', 'block');
          }
        }
        
        return element.css('display', 'none');

        return $rootScope.$on('loginStatusChanged', function (event) {
          var authElements = element[0].children[0].children;
        var allow = false;
        
        for (var i = 0; i < authElements.length; i++) {
          
          if (authElements[i].children[0].hasAttribute("access-claim"))
          {
            allow = securityService.authorizePath(authElements[i].children[0].hash.substring(1, authElements[i].children[0].hash.length));
          }
          else
          {
            allow = true;
          }
          
          if (allow) 
          {
            return element.css('display', 'block');
          }
        }
        
        return element.css('display', 'none');
        });

      }
    };
  }]);


  return app;

});





