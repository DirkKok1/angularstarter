'use strict';

require.config({
  baseUrl: '',
  urlArgs: 'v=1.0'
});

require(
  [
    'app',
    'shared/routeResolver',
    'shared/globalsService',
    'shared/securityService',
    'shared/authService',
    'shared/authInterceptorService',
    'shared/http401InterceptorService',
    'shared/viewStateService',
    'shared/navBar/navBarController',
    'shared/directives/dateDirectives',
    'shared/directives/validators',
    'shared/dataServices/rolesDataService',
    'shared/dataServices/claimsDataService',
    'shared/dataServices/versionDataService',
    'shared/dataServices/usersDataService',
    'shared/dataServices/fileUploadDataService',
    'shared/dataServices/orgNodesDataService',
    'shared/dataServices/ordersDataService'
  ],
  function () {
    angular.bootstrap(document, ['angularStarter']);
  });
