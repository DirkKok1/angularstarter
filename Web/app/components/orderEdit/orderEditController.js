'use strict';

define(['app'], function (app) {

  var injectParams = ['$scope', '$location', '$routeParams', '$q', 'ordersDataService', 'ngDialog', 'globalsService'];

  var orderEditController = function ($scope, $location, $routeParams, $q, ordersDataService, ngDialog, globalsService) {

    var orderId = ($routeParams.orderId) ? $routeParams.orderId : '';

    var vm = this;
    vm.title = (orderId != '') ? "Edit Existing Order" : "Add New Order";

    if (orderId != '') {
      ordersDataService.getOrderById(orderId).success(function (results) {
        vm.order = results;
        setOrderLinesDataSource();        
      }).error(function (error) {
        ngDialog.openConfirm({
            template: "shared/partials/notificationDialog.html",
            data: { Header: 'Unable to load data', Message: 'We are unable to load this record' }
          }).then(function (result) {
            $location.path('/orders');
          });
      });
    }
    else {
      vm.order = {};
      vm.order.OrderLines = [];
      
      setOrderLinesDataSource();
    }

    function setOrderLinesDataSource() {

      vm.orderLinesGrid.dataSource = new kendo.data.DataSource({
        data: vm.order.OrderLines,
        schema: {
          model: {
            id: "Id",
            fields: {
              Description: { type: "string" },
              Qty: { type: "number" },
              Price: { type: "number" },
              Position: { type: "number" }
            }
          }
        },
        pageSize: 10,
        sort: [{ field: "Position", dir: "asc" }]
      });
      vm.orderLinesGrid.dataSource.read();
      vm.orderLinesGrid.refresh();
    }

    vm.save = function() {
      if (!$scope.editForm.$valid)
        return;

        vm.order.OrderDate = moment(vm.order.OrderDate).format();

      saveOrder()
        .then(function (result) {
          $location.path("/orders");
        })
        .catch(function (error){
          ngDialog.openConfirm({
            template: "shared/partials/notificationDialog.html",
            data: { Header: 'Save Error', Message: 'We are unable to save this item' }
          });
        });
    };

    function saveOrder() {
      var deferred = $q.defer();

      if (orderId != '')
      {
        ordersDataService.updateOrder(vm.order).success(function (results) {
          deferred.resolve(vm.order.Id);
        }).error(function (error) {
          deferred.reject(error);
        });
      }
      else
      {
        ordersDataService.addOrder(vm.order).success(function (results) {
          vm.order.Id = results.Id;
          deferred.resolve(vm.order.Id);
        }).error(function (error) {
          deferred.reject(error);
        });
      }

      return deferred.promise;
    }

    vm.deleteOrderLine = function(dataItem) {

      ngDialog.openConfirm({
        template: "shared/partials/confirmationDialog.html",
        data: { Header: 'Remove Order Line', Message: 'Are you sure you want to remove this item?' }
      }).then(function(value){
        vm.orderLinesGrid.dataSource.remove(dataItem);
        reorderOrderLines();
        $scope.editForm.$dirty = true;
      });
    };

    function reorderOrderLines() {

      var currentPos = 1;
      Enumerable.From(vm.orderLinesGrid.dataSource.data()).ForEach(function (item) {
        item.Position = currentPos;
        currentPos++;
      });
      
      vm.orderLinesGrid.refresh();
    }

    vm.moveOrderLineUp = function (dataItem) {

      if (dataItem.Position == 1 || vm.orderLinesGrid.dataSource.total() == 1)
      {
        return;
      }

      var currentItem = globalsService.find(vm.order.OrderLines, "Id", dataItem.Id);
      var prevItem = globalsService.find(vm.order.OrderLines, "Position", dataItem.Position - 1);

      prevItem.Position++;
      currentItem.Position--;

      vm.orderLinesGrid.dataSource.read();
      vm.orderLinesGrid.dataSource.sort({ field: "Position", dir: "asc" });
      vm.orderLinesGrid.refresh();

      $scope.editForm.$dirty = true;

    };

    vm.moveOrderLineDown = function (dataItem) {

      if (dataItem.Position == vm.orderLinesGrid.dataSource.total())
      {
        return;
      }

      var currentItem = globalsService.find(vm.order.OrderLines, "Id", dataItem.Id);
      var nextItem = globalsService.find(vm.order.OrderLines, "Position", dataItem.Position + 1);

      nextItem.Position--;
      currentItem.Position++;

      vm.orderLinesGrid.dataSource.read();
      vm.orderLinesGrid.dataSource.sort({ field: "Position", dir: "asc" });
      vm.orderLinesGrid.refresh();
      
      $scope.editForm.$dirty = true;
    };

    vm.orderLineDescriptionHasDuplicates = function(newDescription) {

      // No duplicate if the description matches the edited day value
      if (newDescription == vm.currentOrderLine.OldDescription) {
        return false;
      }

      // Get the list of original order day names
      var itemsToCheck = Enumerable.From(vm.orderLinesGrid.dataSource.data()).Select("$.Description").ToArray();

      // Add the new order day name to the list
      itemsToCheck.push(newDescription);

      var hasDuplicates = Enumerable.From(itemsToCheck).GroupBy(function (item) { return item; }, function(item) { return item;}, function(item, grouping) { return grouping.Count();}).Any("$ > 1");

      return hasDuplicates;
    };

    vm.editOrderLine = function (dataItem) {

      vm.currentOrderLine = { Description: dataItem.Description, OldDescription: dataItem.Description, Price: dataItem.Price, Qty: dataItem.Qty };

      ngDialog.openConfirm({
        template: "components/orderEdit/orderLineEdit.html",
        scope: $scope
      }).then(function(value){

        dataItem.Description = vm.currentOrderLine.Description;
        dataItem.Price = vm.currentOrderLine.Price;
        dataItem.Qty = vm.currentOrderLine.Qty;

        vm.currentOrderLine = null;
        $scope.editForm.$dirty = true;
      }, function(value){
        vm.currentOrderLine = null;
      });
    };

    vm.addOrderLine = function() {

      vm.currentOrderLine = {Id: globalsService.generateUUID(), Position: vm.orderLinesGrid.dataSource.total() + 1, OrderId: vm.order.Id, Description: '', Qty: 0, Price: 0 };

      ngDialog.openConfirm({
        template: "components/orderEdit/orderLineEdit.html",
        scope: $scope
      }).then(function(value){
        vm.order.OrderLines.push(vm.currentOrderLine);
        vm.currentOrderLine = null;
        vm.orderLinesGrid.dataSource.read();
        vm.orderLinesGrid.refresh();
        $scope.editForm.$dirty = true;

      }, function(value){
        vm.currentOrderLine = null;
      });

    };

    


    vm.cancel = function() {
      $location.path('/orders');
    };

    vm.orderLinesGridOptions = {
      toolbar: [{name: 'Add', text: 'Add Order Line', template: '<button data-ng-click=\'vm.addOrderLine()\' class=\'k-button\'>Add Order Line</button>'} ],
      sortable: true,
      pageable: true,
      selectable: false,
//      autoBind: false,
      columns: [
        { field: 'Description', title: 'Description'},
        { field: 'Price', title: 'Price'},
        { field: 'Qty', title: 'Qty'},
        { template: '<button data-ng-click="vm.deleteOrderLine(dataItem)" class="btn btn-default fa fa-trash-o" tooltip="Delete" tooltip-placement="left"></button>', width: 60, title: ""},
        { template: '<button data-ng-click="vm.editOrderLine(dataItem)" class="btn btn-default fa fa-pencil" tooltip="Edit" tooltip-placement="left"></button>', width: 60, title: ""},
        { template: '<button data-ng-click="vm.moveOrderLineUp(dataItem)" class="btn btn-default fa fa-arrow-up" tooltip="Move Up" tooltip-placement="left"></button>', width: 60, title: ""},
        { template: '<button data-ng-click="vm.moveOrderLineDown(dataItem)" class="btn btn-default fa fa-arrow-down" tooltip="Move Down" tooltip-placement="left"></button>', width: 60, title: ""}
      ]
    };

  };

  orderEditController.$inject = injectParams;

  app.register.controller('OrderEditController', orderEditController);

});
