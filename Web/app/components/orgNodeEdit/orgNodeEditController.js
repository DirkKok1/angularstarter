'use strict';

define(['app'], function (app) {

  var injectParams = ['$scope', '$location', '$routeParams', 'orgNodesDataService', 'ngDialog'];

  var OrgNodeEditController = function ($scope, $location, $routeParams, orgNodesDataService, ngDialog) {

    var orgNodeId = ($routeParams.orgNodeId) ? $routeParams.orgNodeId : '';
    var parentId = $location.search().parentId;

    var vm = this;
    vm.title = (orgNodeId != '') ? "Edit Existing Org Node" : "Add New Org Node";
    vm.orgNode = {};
    if (parentId) {
      vm.orgNode.ParentId = parentId;
    }

    if (orgNodeId != '') {
        orgNodesDataService.getOrgNodeById(orgNodeId).success(function (results) {
          vm.orgNode = results;
        }).error(function (error) {
          ngDialog.openConfirm({
            template: "shared/partials/notificationDialog.html",
            data: { Header: 'Unable to load data', Message: 'We are unable to load this record' }
          }).then(function (result) {
            $location.path('/treeListView');
          });
        });
    }

    vm.save = function() {
      if (!$scope.editForm.$valid)
        return;

      $location.search('parentId', null);

      if (orgNodeId != '')
      {
        orgNodesDataService.updateOrgNode(vm.orgNode).success(function (results) {
          $location.path('/treeListView');
        }).error(function (error) {
          ngDialog.openConfirm({
            template: "shared/partials/notificationDialog.html",
            data: {Header: 'Unable to save data', Message: 'We are unable to save this record'}
          });
        });
      }
      else
      {
        orgNodesDataService.addOrgNode(vm.orgNode).success(function (results) {
          $location.path('/treeListView');
        }).error(function (error) {
          ngDialog.openConfirm({
            template: "shared/partials/notificationDialog.html",
            data: {Header: 'Unable to save data', Message: 'We are unable to save this record'}
          });
        });
      }
    };

    vm.cancel = function() {
      $location.search('parentId', null);
      $location.path('/treeListView');
    };
  };

  OrgNodeEditController.$inject = injectParams;

  app.register.controller('OrgNodeEditController', OrgNodeEditController);

});
