'use strict';

define(['app'], function (app) {

  var injectParams = ['$location', '$routeParams', 'authService', 'securityService'];

  var LoginController = function ($location, $routeParams, authService, securityService) {

    var vm = this;
    var path = '/';
    vm.title = "Login";
    vm.loginData = {
      userName: "",
      password: "",
      useRefreshTokens: true
    };
    vm.message = "";

    vm.login = function () {

      authService.login(vm.loginData).then(function (response) {

        if (!authService.authentication.isAuth) {
          vm.errorMessage = 'Invalid username or password';
          return;
        }

        if ($routeParams && $routeParams.redirect) {
          path = path + $routeParams.redirect;
        }
        //request additional data using acces token
        authService.getUserClaims(response.UserId).then(function (claims) {
          authService.getUserRoles(response.UserId).then(function (roles) {
            if (!$routeParams.redirect) {
              $location.path('/users');
              //visually indicate current location
              var panelBar = $("#nav-panel-bar").data("kendoPanelBar");
              panelBar.expand($("#security"));
              panelBar.select("#users");
              console.log('done');
            } else {
              $location.path(path);
            }
          }, function (err) {
            vm.message = "Unable to connect to server";
          });
        }, function (err) {
          vm.message = "Unable to connect to server";
        });

      }, function (err) {

        if (err == null) {
          vm.message = "Unable to connect to server";
        }
        else {
          vm.message = err.error_description;
        }
      });
    };

  };

  LoginController.$inject = injectParams;

  app.register.controller('LoginController', LoginController);

});
