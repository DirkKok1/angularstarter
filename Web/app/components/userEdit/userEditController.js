'use strict';

define(['app'], function (app) {

  var injectParams = ['$scope', '$location', '$routeParams', 'usersDataService', 'rolesDataService','globalsService', 'ngDialog'];

  var UserEditController = function ($scope, $location, $routeParams, usersDataService, rolesDataService,globalsService, ngDialog) {

    var userId = ($routeParams.userId) ? $routeParams.userId : '';

    var vm = this;
    vm.title = "Edit Existing User";
    vm.dataLoaded = {};
    vm.user = {};

    function init() {
      if (userId != '')
      {
        usersDataService.getUserById(userId).success(function (results) {
          vm.user = results;
          vm.dataLoaded["user"] = true;
          vm.setRolesCheckBoxStates();
        }).error(function (error) {
          ngDialog.openConfirm({
            template: "shared/partials/notificationDialog.html",
            data: { Header: 'Unable to load data', Message: 'We are unable to load this record' }
          }).then(function (result) {
            $location.path('/users');
          });
        });
      }
    }

    init();

    vm.save = function() {
      if (!$scope.editForm.$valid)
        return;

      // do update validations here
      usersDataService.updateUser(vm.user).success(function (results) {
        $location.path('/users');
      }).error(function (error) {
          ngDialog.openConfirm({
            template: "shared/partials/notificationDialog.html",
            data: {Header: 'Unable to save data', Message: 'We are unable to save this record'}
          });
        });

    };

    vm.cancel = function() {
      $location.path('/users');
    };

    vm.onRolesDataBound = function(e) {
      vm.rolesGrid.table.on("click", ".checkbox", vm.onRoleCheckClick);
      vm.dataLoaded["roles"] = true;
      vm.setRolesCheckBoxStates();
    };

    vm.setRolesCheckBoxStates = function() {
      
      if (!vm.dataLoaded["roles"] || !vm.dataLoaded["user"]) return;
      
      var view = vm.rolesDataSource.view();
      for(var i = 0; i < view.length;i++){
        if (globalsService.find(vm.user.Roles, "Id", view[i].id)) {
          vm.rolesGrid.tbody.find("tr[data-uid='" + view[i].uid + "']")
            .find(".checkbox")
            .attr("checked","checked");
        }
      }
    };

    vm.onRoleCheckClick = function() {
      var checked = this.checked;
      var row = $(this).closest("tr");
      var dataItem = vm.rolesGrid.dataItem(row);

      var selectedItem = globalsService.find(vm.user.Roles, "Id", dataItem.Id);
      if (checked)
      {
        if (!selectedItem)
        {
          vm.user.Roles.push({ Id: dataItem.Id, Name: dataItem.Name, CanAccessAllModules: dataItem.CanAccessAllModules, CanAccessAllData: dataItem.CanAccessAllData });
        }
      }
      else
      {
        if (selectedItem)
        {
          var indexToRemove = vm.user.Roles.indexOf(selectedItem);
          vm.user.Roles.splice(indexToRemove, 1);
        }
      }


      $scope.editForm.$dirty = true;
      $scope.$apply();
    };

    vm.rolesDataSource = new kendo.data.DataSource({
      type: "odata",
      transport: {
        read: {
          async: true,
          url: rolesDataService.serviceBaseUrl,
          beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization',globalsService.getTokenHeader())
          },
          dataType: "json"
        },
        parameterMap: function (options, type) {
          var d = kendo.data.transports.odata.parameterMap(options);
          delete d.$inlinecount; // <-- remove inlinecount parameter
          d.$count = true;
          return d;
        }
      },
      batch: false,
      serverPaging: true,
      serverSorting: true,
      serverFiltering: true,
      pageSize: 10,
      schema: {
        data: function (data) { return data.value; },
        total: function (data) { return data["@odata.count"]; },
        model: {
          id: "Id",
          fields: {
            Id: {type: "guid", editable: false, nullable: false},
            Name: {title: "Name", type: "string"}
          }
        }
      },
      sort: [{ field: "Name", dir: "asc" }],
    });

    vm.rolesGridOptions = {
      dataSource: vm.rolesDataSource,
      sortable: true,
      pageable: true,
      selectable: false,
      dataBound: vm.onRolesDataBound,
      columns: [
        { template: "<input type='checkbox' class='checkbox' />", width: 100 },
        { field: "Id", title: "Id", hidden:true},
        { field: "Name", title: "Name"}
      ]
    };

  };

  UserEditController.$inject = injectParams;

  app.register.controller('UserEditController', UserEditController);

});
