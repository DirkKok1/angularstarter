'use strict';

define(['app'], function (app) {

  var injectParams = ['$scope', '$location', '$routeParams', 'usersDataService','globalsService', 'ngDialog', 'localStorageService'];

  var ManageAccountController = function ($scope, $location, $routeParams, usersDataService, globalsService, ngDialog, localStorageService) {

    var vm = this;
    vm.title = "Manage Account";
    vm.user = {};

    function init() {
      
      var authData = localStorageService.get('authorizationData');
      
        usersDataService.getUserById(authData.userId).success(function (results) {
          vm.user = results;
        }).error(function (error) {
          ngDialog.openConfirm({
            template: "shared/partials/notificationDialog.html",
            data: { Header: 'Unable to load data', Message: 'We are unable to load this record' }
          }).then(function (result) {
            $location.path('/manageAccount');
          });
        });
    }

    init();

    vm.save = function() {
      if (!$scope.editForm.$valid)
        return;

      // do update validations here
      usersDataService.updateUser(vm.user).success(function (results) {
        ngDialog.openConfirm({
            template: "shared/partials/notificationDialog.html",
            data: {Header: 'Save Complete', Message: 'Your changes have been saved'}
          });
      }).error(function (error) {
          ngDialog.openConfirm({
            template: "shared/partials/notificationDialog.html",
            data: {Header: 'Unable to save data', Message: 'We are unable to save this record'}
          });
        });

    };

    vm.cancel = function() {
      $location.path('/manageAccount');
    };
  };

  ManageAccountController.$inject = injectParams;

  app.register.controller('ManageAccountController', ManageAccountController);

});
