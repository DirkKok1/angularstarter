'use strict';

define(['app'], function (app) {

  var injectParams = ['$scope', '$location', '$routeParams', 'orgNodesDataService', 'viewStateService', 'ngDialog', 'globalsService'];

  var TreeViewController = function ($scope, $location, $routeParams, orgNodesDataService, viewStateService, ngDialog, globalsService) {

    var vm = this;

    vm.title = "View Hierachical Data";
    vm.orgNodeSelected = viewStateService.selectedOrgNodeId && viewStateService.selectedOrgNodeId != '';

    function onOrgNodeExpand(e) {

      var dataItem = vm.orgNodesTreeView.dataItem(e.node);

      if (dataItem && dataItem.expanded && dataItem.Id) {
        viewStateService.storeOrgNodeExpandedState(dataItem.Id, true);
      }
    }

    function onOrgNodeCollapse(e) {
      var dataItem = vm.orgNodesTreeView.dataItem(e.node);
      if (dataItem && dataItem.Id) {
        viewStateService.storeOrgNodeExpandedState(dataItem.Id, false);
      }
    }

    function onOrgNodesDataBound(e) {
      var data;
      if (e.node) {
        data = vm.orgNodesTreeView.dataItem(e.node).children.data();
      }
      else {
        data = vm.orgNodesTreeViewDataSource.data();
      }

      for (var i = 0; i < data.length; i++) {
        var node = vm.orgNodesTreeView.findByUid(data[i].uid);
        if (viewStateService.orgNodeExpandedStates[data[i].Id]) {
          vm.orgNodesTreeView.expand(node);
        }

        if (viewStateService.selectedOrgNodeId == data[i].Id) {
          vm.orgNodesTreeView.select(node);
          vm.orgNodeSelected = true;
        }
      }
    }

    vm.orgNodesTreeViewDataSource = new kendo.data.HierarchicalDataSource({
      type: "odata",
      transport: {
        read: {
          async: true,
          url: orgNodesDataService.serviceBaseUrl,
          beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', globalsService.getTokenHeader())
          },
          dataType: "json"
        },
        parameterMap: function (options, type) {
          var d = kendo.data.transports.odata.parameterMap(options);
          delete d.$inlinecount; // <-- remove inlinecount parameter
          d.$count = true;
          return d;
        }
      }
      , schema: {
        data: function (data) {
          return data.value;
        }
        , total: function (data) {
          return data["@odata.count"];
        }

        , model: {
          id: "Id",
          hasChildren: true,
          fields: {
            parentId: {field: "ParentId", type: "string", nullable: true},
            Id: {field: "Id", type: "string"},
            Name: {field: "Name"}
          }
        }
      },
      sort: [{field: "Name", dir: "asc"}],
      serverSorting: true
    });

    vm.orgNodesTreeViewOptions = {
      dataSource: vm.orgNodesTreeViewDataSource,
      loadOnDemand: true,
      expand: onOrgNodeExpand,
      collapse: onOrgNodeCollapse,
      dataBound: onOrgNodesDataBound,
      dataTextField: "Name"

    };

  };

  TreeViewController.$inject = injectParams;

  app.register.controller('TreeViewController', TreeViewController);

})
;
