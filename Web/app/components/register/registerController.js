'use strict';

define(['app'], function (app) {

  var injectParams = ['$location', '$timeout', 'authService'];

  var RegisterController = function ($location, $timeout, authService) {

    var vm = this;

    vm.title = "Register User";
    vm.savedSuccessfully = false;
    vm.message = "";

    vm.registration = {
      userName: "",
      password: "",
      confirmPassword: ""
    };

    vm.registerUser = function() {

      authService.saveRegistration(vm.registration).then(function (response) {

        vm.savedSuccessfully = true;
        vm.message = "User has been registered successfully, you will be redirected to the login page in 2 seconds.";
        authService.logOut();
        vm.startTimer();
      }, function(response) {

          var errors = [];
          for(var key in response.data.modelState) {
            for (var i = 0; i < response.data.modelState[key].length; i++) {
              errors.push(response.data.modelState[key][i]);
            }
          }
          vm.message = "Failted to register user due to:" + errors.join(' ');
        });

    };

    vm.startTimer = function() {

      var timer = $timeout(function() {

        $timeout.cancel(timer);
        $location.path('/login');

      }, 2000);
    };

  };

  RegisterController.$inject = injectParams;

  app.register.controller('RegisterController', RegisterController);

});
