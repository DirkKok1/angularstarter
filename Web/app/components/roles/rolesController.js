'use strict';

define(['app'], function (app) {

  var injectParams = ['$scope', '$rootScope', '$location', 'rolesDataService', 'ngDialog', 'globalsService', 'localStorageService'];

  var RolesController = function ($scope, $rootScope, $location, rolesDataService, ngDialog, globalsService, localStorageService) {

    var vm = this;
    vm.title = "Manage Roles";

    vm.currentUserRoles = localStorageService.get('userRoles');

    vm.edit = function (dataItem) {
      $location.path('/roleEdit/' + dataItem.Id);
    };

    vm.add = function () {
      $location.path('/roleEdit');
    }

    vm.delete = function (dataItem) {

      ngDialog.openConfirm({
        template: "shared/partials/confirmationDialog.html",
        data: {Header: 'Delete Role', Message: 'Are you sure you want to delete this item?'}
      }).then(function () {
        rolesDataService.deleteRoleById(dataItem.Id).success(function () {
          vm.dataSource.remove(dataItem);
        }).error(function () {
          ngDialog.openConfirm({
            template: "shared/partials/notificationDialog.html",
            data: {Header: 'Unable to Delete Item', Message: 'We are unable to delete this item'}
          });
        });
      });

    };

    vm.dataSource = new kendo.data.DataSource({
      type: "odata",
      transport: {
        read: {
          async: true,
          url: rolesDataService.serviceBaseUrl,
          beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', globalsService.getTokenHeader())
          },
          dataType: "json"
        },
        parameterMap: function (options, type) {
          var d = kendo.data.transports.odata.parameterMap(options);
          delete d.$inlinecount; // <-- remove inlinecount parameter
          d.$count = true;
          return d;
        }
      },
      batch: false,
      serverPaging: true,
      serverSorting: true,
      serverFiltering: true,
      pageSize: 10,
      schema: {
        data: function (data) {
          return data.value;
        },
        total: function (data) {
          return data["@odata.count"];
        },
        model: {
          id: "Id",
          fields: {
            Name: {title: "Name", type: "string"}
          }
        }
      },
      sort: [{field: "Name", dir: "asc"}]
    });

    vm.currentUserMemberOfRole = function (roleId) {
      
      if (globalsService.find(vm.currentUserRoles, "Id", roleId))
        return true;
      else
        return false;
    }


    vm.rolesGridOptions = {
      dataSource: vm.dataSource,
      sortable: true,
      pageable: true,
      selectable: false,
      toolbar: [{
        name: 'Add',
        text: 'Add Role',
        template: '<button data-ng-click=\'vm.add()\' class=\'k-button\'>Add Role</button>'
      }, "excel"],
      excel: {
        fileName: "Roles.xlsx",
        allPages: true
      },
      columns: [
        {field: 'Name', title: 'Name'},
        {
          template: '<button ng-disabled="vm.currentUserMemberOfRole(dataItem.Id)" data-ng-click="vm.delete(dataItem)" class="btn btn-default fa fa-trash-o" tooltip="Delete" tooltip-placement="left"></button>',
          width: 60,
          title: ""
        },
        {
          template: '<button data-ng-click="vm.edit(dataItem)" class="btn btn-default fa fa-pencil" tooltip="Edit" tooltip-placement="left"></button>',
          width: 60,
          title: ""
        }
      ]
    };


  };

  RolesController.$inject = injectParams;

  app.register.controller('RolesController', RolesController);

});
