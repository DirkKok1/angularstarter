'use strict';

define(['app'], function (app) {

    var injectParams = ['$scope', '$rootScope', '$location', 'ordersDataService', 'ngDialog', 'globalsService'];

    var OrdersController = function ($scope, $rootScope, $location, ordersDataService, ngDialog, globalsService) {

    var vm = this;
    vm.title = "Manage Orders";

    vm.edit = function (dataItem) {
      $location.path('/orderEdit/' + dataItem.Id);
    };

    vm.add = function() {
      $location.path('/orderEdit');
    }

    vm.delete = function (dataItem) {

      ngDialog.openConfirm({
            template: "shared/partials/confirmationDialog.html",
            data: { Header: 'Delete Item', Message: 'Are you sure you want to delete this item?' }
          }).then(function () {
            ordersDataService.deleteOrderById(dataItem.Id).success(function () {
              vm.dataSource.remove(dataItem);
            }).error(function () {
              ngDialog.openConfirm({
                template: "shared/partials/notificationDialog.html",
                data: { Header: 'Unable to Delete Item', Message: 'We are unable to delete this item' }
              });
            });
          });

        };

    vm.dataSource = new kendo.data.DataSource({
      type: "odata",
      transport: {
        read: {
          async: true,
          url: ordersDataService.serviceBaseUrl,
          beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization',globalsService.getTokenHeader())
          },
          dataType: "json"
        },
        parameterMap: function (options, type) {
          var d = kendo.data.transports.odata.parameterMap(options);
          delete d.$inlinecount; // <-- remove inlinecount parameter
          d.$count = true;
          return d;
        }
      },
      batch: false,
      serverPaging: true,
      serverSorting: true,
      serverFiltering: true,
      pageSize: 10,
      schema: {
        data: function (data) { return data.value; },
        total: function (data) { return data["@odata.count"]; },
        model: {
          id: "Id",
          fields: {
            Name: {title: "Name", type: "string"}
          }

        }
      },
      sort: [{ field: "Name", dir: "asc" }]
    });


    vm.ordersGridOptions = {
      dataSource: vm.dataSource,
      sortable: true,
      pageable: true,
      selectable: false,
      toolbar: [{name: 'Add', text: 'Add Order', template: '<button data-ng-click=\'vm.add()\' class=\'k-button\'>Add Order</button>'}, "excel"],
      excel: {
        fileName: "Orders.xlsx",
        allPages: true
      },
      columns: [
        {
          field: 'Name', title: 'Name'
        },
               { template: '<button data-ng-click="vm.delete(dataItem)" class="btn btn-default fa fa-trash-o" tooltip="Delete" tooltip-placement="left"></button>', width: 60, title: ""},
               { template: '<button data-ng-click="vm.edit(dataItem)" class="btn btn-default fa fa-pencil" tooltip="Edit" tooltip-placement="left"></button>', width: 60, title: ""}
      ]
    };

  };

  OrdersController.$inject = injectParams;

  app.register.controller('OrdersController', OrdersController);

});
