'use strict';

define(['app'], function (app) {

  var injectParams = ['$scope', '$location', '$routeParams', 'localStorageService', 'authService', 'globalsService', 'ngDialog'];

  var ChangePasswordController = function ($scope, $location, $routeParams, localStorageService, authService, globalsService, ngDialog) {

    var vm = this;
    vm.title = "Change Password";
    
    var authData = localStorageService.get('authorizationData');
    
    vm.user = {
      UserId: authData.userId,
      CurrentPassword: undefined,
      NewPassword: undefined,
      ConfirmPassword: undefined
    };

    vm.save = function () {
      if (!$scope.editForm.$valid)
        return;

      authService.changePassword(vm.user).then(function (response) {
        
        authService.logOut();
        $location.path('/login');
        
      }, function (response) {

        ngDialog.openConfirm({
          template: "shared/partials/notificationDialog.html",
          data: { Header: 'Unable to save data', Message: 'We are unable to save this record' }
        });
      });

    };

    vm.cancel = function () {
      $location.path('/changePassword');
    };

  };

  ChangePasswordController.$inject = injectParams;

  app.register.controller('ChangePasswordController', ChangePasswordController);

});
