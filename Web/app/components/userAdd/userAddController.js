'use strict';

define(['app'], function (app) {

  var injectParams = ['$scope', '$location', '$routeParams', 'usersDataService', 'rolesDataService', 'authService', 'globalsService', 'ngDialog'];

  var UserAddController = function ($scope, $location, $routeParams, usersDataService, rolesDataService, authService, globalsService, ngDialog) {

    var vm = this;
    vm.title = "Add New User";
    vm.user = {};
    vm.user.Roles = [];

    vm.save = function () {
      if (!$scope.editForm.$valid)
        return;

      authService.saveRegistration(vm.user).then(function (response) {
        $location.path('/users');
      }, function (response) {

        ngDialog.openConfirm({
          template: "shared/partials/notificationDialog.html",
          data: { Header: 'Unable to save data', Message: 'We are unable to save this record' }
        });
      });

    };

    vm.cancel = function () {
      $location.path('/users');
    };

    vm.onRolesDataBound = function (e) {
      vm.rolesGrid.table.on("click", ".checkbox", vm.onRoleCheckClick);
    };

    vm.onRoleCheckClick = function () {
      var checked = this.checked;
      var row = $(this).closest("tr");
      var dataItem = vm.rolesGrid.dataItem(row);

      var selectedItem = globalsService.find(vm.user.Roles, null, dataItem.Id);
      if (checked) {
        if (!selectedItem) {
          vm.user.Roles.push(dataItem.Id);
        }
      }
      else {
        if (selectedItem) {
          var indexToRemove = vm.user.Roles.indexOf(selectedItem);
          vm.user.Roles.splice(indexToRemove, 1);
        }
      }

      $scope.editForm.$dirty = true;
      $scope.$apply();
    };

    vm.rolesDataSource = new kendo.data.DataSource({
      type: "odata",
      transport: {
        read: {
          async: true,
          url: rolesDataService.serviceBaseUrl,
          beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', globalsService.getTokenHeader())
          },
          dataType: "json"
        },
        parameterMap: function (options, type) {
          var d = kendo.data.transports.odata.parameterMap(options);
          delete d.$inlinecount; // <-- remove inlinecount parameter
          d.$count = true;
          return d;
        }
      },
      batch: false,
      serverPaging: true,
      serverSorting: true,
      serverFiltering: true,
      pageSize: 10,
      schema: {
        data: function (data) { return data.value; },
        total: function (data) { return data["@odata.count"]; },
        model: {
          id: "Id",
          fields: {
            Id: { type: "guid", editable: false, nullable: false },
            Name: { title: "Name", type: "string" }
          }
        }
      },
      sort: [{ field: "Name", dir: "asc" }],
    });

    vm.rolesGridOptions = {
      dataSource: vm.rolesDataSource,
      sortable: true,
      pageable: true,
      selectable: false,
      dataBound: vm.onRolesDataBound,
      columns: [
        { template: "<input type='checkbox' class='checkbox' />", width: 100 },
        { field: "Id", title: "Id", hidden: true },
        { field: "Name", title: "Name" }
      ]
    };

  };

  UserAddController.$inject = injectParams;

  app.register.controller('UserAddController', UserAddController);

});
