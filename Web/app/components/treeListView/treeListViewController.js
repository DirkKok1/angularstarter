'use strict';

define(['app'], function (app) {

  var injectParams = ['$scope', '$rootScope', '$location', 'orgNodesDataService', 'ngDialog', 'viewStateService', 'globalsService'];

  var TreeListViewController = function ($scope, $rootScope, $location, orgNodesDataService, ngDialog, viewStateService, globalsService) {

    var vm = this;

    vm.title = "Manage Hierachical Data";

    vm.edit = function (dataItem) {
      $location.path('/orgNodeEdit/' + dataItem.Id);
    };

    vm.addRoot = function () {
      $location.search('parentId', null);
      $location.path('/orgNodeEdit');
    }

    vm.addChild = function (dataItem) {
      viewStateService.storeOrgNodeExpandedState(dataItem.Id, true);
      $location.search('parentId', dataItem.Id);
      $location.path('/orgNodeEdit');
    };

    vm.delete = function (dataItem) {

      ngDialog.openConfirm({
        template: "shared/partials/confirmationDialog.html",
        data: { Header: 'Delete Org Node', Message: 'Are you sure you want to delete this item?' }
      }).then(function () {
        orgNodesDataService.deleteOrgNodeById(dataItem.Id).success(function () {
          vm.treeListDataSource.remove(dataItem);
          viewStateService.storeOrgNodeExpandedState(dataItem.Id, false);
        }).error(function () {
          ngDialog.openConfirm({
            template: "shared/partials/notificationDialog.html",
            data: { Header: 'Unable to Delete Item', Message: 'We are unable to delete this item' }
          });
        });
      });

    };

    function onOrgNodeExpand(e) {
      if (e.model && e.model.Id)
      {
        viewStateService.storeOrgNodeExpandedState(e.model.Id, true);
      }
    }

    function onOrgNodeCollapse(e) {

      if (e.model && e.model.Id)
      {
        viewStateService.storeOrgNodeExpandedState(e.model.Id, false);
      }
    }

    function setOrgNodesExpandedState(nodes) {
      for(var i = 0; i < nodes.length; i++) {
        var node = nodes[i];

        var childNodes = vm.treeListDataSource.childNodes(node);
        setOrgNodesExpandedState(childNodes);

        if (viewStateService.orgNodeExpandedStates[node.Id])
        {
          var row = vm.treeList.content.find("tr[data-uid=" + node.uid + "]")
          vm.treeList.expand(row);
        }
      }
    }

    function onOrgNodesDataBound(e) {

      var rootNodes = vm.treeListDataSource.rootNodes();
      setOrgNodesExpandedState(rootNodes);
    }

    vm.treeListDataSource = new kendo.data.TreeListDataSource({
        type: "odata",
        transport: {
          read: {
            async: true,
            url: orgNodesDataService.serviceBaseUrl,
            beforeSend: function(xhr) {
              xhr.setRequestHeader('Authorization',globalsService.getTokenHeader())
            },
            dataType: "json"
          },
          parameterMap: function (options, type) {
            var d = kendo.data.transports.odata.parameterMap(options);
            delete d.$inlinecount; // <-- remove inlinecount parameter
            d.$count = true;
            return d;
          }
        }
        , schema: {
          data: function (data) {
            return data.value;
          }
          , total: function (data) {
            return data["@odata.count"];
          }

          , model: {
            id: "Id",
            hasChildren: true,
            children: "Children",
            fields: {
              parentId: {field: "ParentId", type: "string", nullable: true},
              Id: {field: "Id", type: "string"},
              Name: {field: "Name"}
            }
          }
        },
        sort: [{ field: "Name", dir: "asc" }],
        serverSorting: true,
        serverFiltering: false
      }
    );

    vm.treeListOptions = {
      dataSource: vm.treeListDataSource,
      loadOnDemand: true,
      sortable: true,
      filterable: false,
      columnMenu: true,
      selectable: false,
      editable: false,
      expand: onOrgNodeExpand,
      collapse: onOrgNodeCollapse,
      dataBound: onOrgNodesDataBound,
      columns: [
        {field: "Id", title: "Id"},
        {field: "Name", title: "Name"},
        { template: '<button data-ng-click="vm.delete(dataItem)" class="btn btn-default fa fa-trash-o" tooltip="Delete" tooltip-placement="left"></button>', width: 60, title: ""},
        { template: '<button data-ng-click="vm.edit(dataItem)" class="btn btn-default fa fa-pencil" tooltip="Edit" tooltip-placement="left"></button>', width: 60, title: ""},
        { template: '<button data-ng-click="vm.addChild(dataItem)" class="btn btn-default fa fa-plus" tooltip="Add Child" tooltip-placement="left"></button>', width: 60, title: ""}

      ]
    };
  };

  TreeListViewController.$inject = injectParams;

  app.register.controller('TreeListViewController', TreeListViewController);

})
;
