'use strict';

define(['app'], function (app) {

  var injectParams = ['$scope', '$q', '$location', '$routeParams', 'rolesDataService', 'claimsDataService', 'globalsService', 'ngDialog'];

  var RoleEditController = function ($scope, $q, $location, $routeParams, rolesDataService, claimsDataService, globalsService, ngDialog) {

    var roleId = ($routeParams.roleId) ? $routeParams.roleId : '';

    var vm = this;
    vm.title = (roleId != '') ? "Edit Existing Role" : "Add New Role";
    vm.dataLoaded = {};

    function init() {
      if (roleId != '') {
        rolesDataService.getRoleById(roleId).success(function (results) {
          vm.role = results;
          vm.dataLoaded["role"] = true;
          setClaimsCheckBoxStates();
        }).error(function (error) {
          ngDialog.openConfirm({
            template: "shared/partials/notificationDialog.html",
            data: { Header: 'Unable to load data', Message: 'We are unable to load this record' }
          }).then(function (result) {
            $location.path('/roles');
          });
        });
      }
      else {
        vm.role = {};
        vm.role.Claims = [];
        vm.role.CanAccessAllModules = true;
        vm.role.CanAccessAllData = true;
        vm.dataLoaded["role"] = true;
      }
    }

    init();

    vm.save = function() {
      if (!$scope.editForm.$valid)
        return;

      if (vm.role.CanAccessAllModules)
      {
        // Add all the claims to the role
        vm.role.Claims.length = 0;
        var view = vm.claimsDataSource.view();
        for(var i = 0; i < view.length;i++){
          vm.role.Claims.push({Id: view[i].Id, Name: view[i].Name, ClaimType: view[i].ClaimType, ClaimValue: view[i].ClaimValue});
        }
      }

      if (roleId != '')
      {
        // do update validations here
        rolesDataService.updateRole(vm.role).success(function (results) {
          $location.path('/roles');
              }).error(function (error) {
          ngDialog.openConfirm({
            template: "shared/partials/notificationDialog.html",
            data: {Header: 'Unable to save data', Message: 'We are unable to save this record'}
          });
        });

      }
      else
      {
        // Do new validations here
        rolesDataService.addRole(vm.role).success(function (results) {
          vm.role.Id = results.Id;
          $location.path('/roles');
              }).error(function (error) {
          ngDialog.openConfirm({
            template: "shared/partials/notificationDialog.html",
            data: {Header: 'Unable to save data', Message: 'We are unable to save this record'}
          });
        });

      }


    };

    vm.cancel = function() {
      $location.path('/roles');
    };

    vm.onClaimsDataBound = function (e)
    {
      vm.claimsGrid.table.on("click", ".checkbox", vm.onClaimCheckClick);
      vm.dataLoaded["claims"] = true;
      setClaimsCheckBoxStates();
    }

    function setClaimsCheckBoxStates() {
      
      if (!vm.dataLoaded["claims"] || !vm.dataLoaded["role"]) return;
      
      var view = vm.claimsDataSource.view();
      for(var i = 0; i < view.length;i++){
        
        if (globalsService.find(vm.role.Claims, "Id", view[i].id)) {
          vm.claimsGrid.tbody.find("tr[data-uid='" + view[i].uid + "']")
            .find(".checkbox")
            .attr("checked","checked");
        }
      }
    };

    vm.onClaimCheckClick = function () {
      var checked = this.checked;
      var row = $(this).closest("tr");
      var dataItem = vm.claimsGrid.dataItem(row);

      var selectedItem = globalsService.find(vm.role.Claims, "Id", dataItem.Id);
      if (checked)
      {
        if (!selectedItem)
        {
          vm.role.Claims.push({Id: dataItem.Id, Name: dataItem.Name, ClaimType: dataItem.ClaimType, ClaimValue: dataItem.ClaimValue});
        }
      }
      else
      {
        if (selectedItem)
        {
          var indexToRemove = vm.role.Claims.indexOf(selectedItem);
          vm.role.Claims.splice(indexToRemove, 1);
        }
      }

      $scope.editForm.$dirty = true;
      $scope.$apply();
    };

    vm.claimsDataSource = new kendo.data.DataSource({
      type: "odata",
      transport: {
        read: {
          async: true,
          url: claimsDataService.serviceBaseUrl,
          beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization',globalsService.getTokenHeader())
          },
          dataType: "json"
        },
        parameterMap: function (options, type) {
          var d = kendo.data.transports.odata.parameterMap(options);
          delete d.$inlinecount; // <-- remove inlinecount parameter
          d.$count = true;
          return d;
        }
      },
      batch: false,
      serverPaging: true,
      serverSorting: true,
      serverFiltering: true,
      pageSize: 10,
      schema: {
        data: function (data) { return data.value; },
        total: function (data) { return data["@odata.count"]; },
        model: {
          id: "Id",
          fields: {
            Id: {type: "guid", editable: false, nullable: false},
            Name: {title: "Name", type: "string"}
          }
        }
      },
      sort: [{ field: "Name", dir: "asc" }],
    });

    vm.claimsGridOptions = {
      dataSource: vm.claimsDataSource,
      sortable: true,
      pageable: true,
      selectable: false,
      dataBound: vm.onClaimsDataBound,
      columns: [
        { template: "<input type='checkbox' class='checkbox' />", width: 100 },
        { field: "Id", title: "Id", hidden:true},
        { field: "Name", title: "Name"}
      ]
    };
  };

    RoleEditController.$inject = injectParams;

  app.register.controller('RoleEditController', RoleEditController);

});
