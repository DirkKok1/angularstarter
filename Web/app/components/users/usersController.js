'use strict';

define(['app'], function (app) {

  var injectParams = ['$scope', '$rootScope', '$location', 'usersDataService', 'ngDialog','globalsService','localStorageService','$injector'];

  var UsersController = function ($scope, $rootScope, $location, usersDataService, ngDialog,globalsService,localStorageService,$injector) {

    var vm = this;
    vm.title = "Manage Users";

    vm.authData =  localStorageService.get('authorizationData');

    vm.edit = function (dataItem) {
      $location.path('/userEdit/' + dataItem.Id);
    };

    vm.add = function() {
      $location.path('/userAdd');
    }

    vm.delete = function(dataItem) {

      ngDialog.openConfirm({
        template: "shared/partials/confirmationDialog.html",
        data: { Header: 'Delete User', Message: 'Are you sure you want to delete this item?' }
      }).then(function () {
        usersDataService.deleteUserById(dataItem.Id).success(function () {
          vm.dataSource.remove(dataItem);
        }).error(function () {
          ngDialog.openConfirm({
            template: "shared/partials/notificationDialog.html",
            data: { Header: 'Unable to Delete Item', Message: 'We are unable to delete this item' }
          });
        });
      });

    };

    vm.dataSource = new kendo.data.DataSource({
      type: "odata",
      transport: {
        read: {
          async: true,
          url: usersDataService.serviceBaseUrl,
          beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization',globalsService.getTokenHeader())
          },
          dataType: "json"
        },
        parameterMap: function (options, type) {
          var d = kendo.data.transports.odata.parameterMap(options);
          delete d.$inlinecount; // <-- remove inlinecount parameter
          d.$count = true;
          return d;
        }
      },
      batch: false,
      serverPaging: true,
      serverSorting: true,
      serverFiltering: true,
      pageSize: 10,
      schema: {
        data: function (data) { return data.value; },
        total: function (data) { return data["@odata.count"]; },
        model: {
          id: "Id",
          fields: {
            UserName: {title: "UserName", type: "string"}
          }
        }
      },
      sort: [{ field: "UserName", dir: "asc" }],
      error: function (e)
      {
        e.sender.cancelChanges();
      }
    });


    vm.usersGridOptions = {
      dataSource: vm.dataSource,
      sortable: true,
      pageable: true,
      selectable: false,
      toolbar: [{name: 'Add', text: 'Add User', template: '<button data-ng-click=\'vm.add()\' class=\'k-button\'>Add User</button>'}, "excel"],
      excel: {
        fileName: "Users.xlsx",
        allPages: true
      },
      columns: [
        { field: 'UserName', title: 'User Name' },
        { field: 'FirstName', title: 'First Name' },
        { field: 'LastName', title: 'Last Name' },
        { field: 'Email', title: 'Email' },
        { template: '<button ng-disabled="dataItem.Id ==  vm.authData.userId" data-ng-click="vm.delete(dataItem)" class="btn btn-default fa fa-trash-o" tooltip="Delete" tooltip-placement="left"></button>', width: 60, title: ""},
        { template: '<button  data-ng-click="vm.edit(dataItem)" class="btn btn-default fa fa-pencil" tooltip="Edit" tooltip-placement="left"></button>', width: 60, title: ""}
      ]
    };

  };

  UsersController.$inject = injectParams;

  app.register.controller('UsersController', UsersController);

});
