'use strict';

define(['app'], function (app) {

  var injectParams = ['$q', '$location', '$rootScope', 'localStorageService','$http'];

  var securityService = function ($q, $location, $rootScope, localStorageService,$http) {

    var factory = {};
    var user = null;

    factory.authorizePage = function (page) {
      if(!page || !page.originalPath)return true;

      var pageClaims = localStorageService.get('userPageClaims');
      var nextPage = page.originalPath;
      if (page.originalPath.indexOf("/:") != -1) {
        nextPage = page.originalPath.substring(0, nextPage.indexOf("/:"));
      }
      return Enumerable.From(pageClaims).Contains(nextPage, "$.Route");
    }

    factory.authorizePath = function (path) {
      if(!path)return true;

      var pageClaims = localStorageService.get('userPageClaims');
      var nextPage = path;
      if (path.indexOf("/:") != -1) {
        nextPage = path.substring(0, nextPage.indexOf("/:"));
      }
      return Enumerable.From(pageClaims).Contains(nextPage, "$.Route");
    }

    factory.loggedIn = function () {
      return localStorageService.get('currentUser') != null;
    }
    factory.setCurrentUser = function (newUser) {
      user = newUser;
    }

    return factory;
  };

  securityService.$inject = injectParams;

  app.factory('securityService', securityService);

});
