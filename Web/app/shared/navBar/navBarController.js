'use strict';

define(['app'], function (app) {

  var injectParams = ['$scope', '$location', 'authService','versionDataService'];

  var NavbarController = function ($scope, $location, authService, versionDataService) {
    var vm = this;

    vm.appTitle = 'Angular Starter';
    vm.versionInfo = undefined;
    vm.authentication = authService.authentication;

    vm.navBarOptions = {
      expandMode: "single"
    };

    vm.redirectToUsers = function(){
      $location.replace();
      $location.path('/users');
    }

    vm.logOut = function () {
      authService.logOut();
      $location.path('/login');
    }

    function loadVersionInfo(){
      versionDataService.getVersion().success(function (results) {
        vm.versionInfo = results;
      }).error(function (error) {
        vm.versionInfo = undefined;
      });
    }

    loadVersionInfo();

    function redirectToLogin() {
      var path = '/login' + $location.$$path;
      $location.replace();
      $location.path(path);
    }

    $scope.$on('redirectToLogin', function () {
      redirectToLogin();
    })

    $scope.$on('loginStatusChanged', function () {

      vm.authentication = authService.authentication;
    });

  };

  NavbarController.$inject = injectParams;

  //Loaded normally since the script is loaded upfront
  //Dynamically loaded controller use app.register.controller
  app.controller('NavbarController', NavbarController);
});
