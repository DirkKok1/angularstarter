'use strict';

define(['app'], function (app) {

  var injectParams = ['$http', '$q', '$rootScope','$location', 'globalsService', 'localStorageService', 'viewStateService','securityService'];

  var authFactory = function ($http, $q, $rootScope,$location, globalsService, localStorageService, viewStateService,securityService) {

    var serviceBase = globalsService.url('api');
    var factory = {};

    factory.authentication = {
      isAuth: false,
      userName: ""
    };

    factory.saveRegistration = function (registration) {
      return $http.post(serviceBase + 'api/account/register', registration).then(function (response) {
        return response;
      });
    };
    
    factory.changePassword = function(newPasswordInfo) {
      return $http.post(serviceBase + 'api/account/changePassword', newPasswordInfo).then(function (response) {
        return response;
      });
    };

    factory.login = function (loginData) {
      var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

      if (loginData.useRefreshTokens) {
        data = data + "&client_id=" + "ngAuthApp";
      }

      var deferred = $q.defer();

      $http.post(serviceBase + 'token', data, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).success(function (response) {

        localStorageService.set('authorizationData', {
          token: response.access_token, 
          username: loginData.userName, 
          userId: response.UserId,
          refreshToken: response.refresh_token, 
          useRefreshTokens: true
          });
        factory.authentication.isAuth = true;
        factory.authentication.userName = loginData.userName;

        localStorageService.set('currentUser',response.UserName);
        deferred.resolve(response);
      }).error(function (err, status) {
        factory.logOut();
        deferred.reject(err);
      });
      return deferred.promise;
    };

    factory.refreshToken = function () {
      var deferred = $q.defer();

      var authData = localStorageService.get('authorizationData');

      if (authData && authData.useRefreshTokens) {

        var data = "grant_type=refresh_token&refresh_token=" + authData.refreshToken + "&client_id=" + "ngAuthApp";

        localStorageService.remove('authorizationData');

        $http = $http || $injector.get('$http');
        $http.post(serviceBase + 'token', data, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).success(function (response) {

          localStorageService.set('authorizationData', {
          token: response.access_token, 
          username: response.userName, 
          userId: response.UserId,
          refreshToken: response.refresh_token, 
          useRefreshTokens: true
          });

          deferred.resolve(response);

        }).error(function (err, status) {
          factory.logOut();
          $location.path('/login');
          deferred.reject(err);
        });
      } else {
        deferred.reject();
      }

      return deferred.promise;
    };

    factory.logOut = function () {
      viewStateService.clear();
      localStorageService.remove('authorizationData');

      factory.authentication.isAuth = false;
      factory.authentication.userName = "";
      localStorageService.set('currentUser',null);
      $rootScope.$broadcast('loginStatusChanged', null);
    };

    factory.getUserClaims = function (userId) {
      var deferred = $q.defer();

      $http.get(serviceBase +  'api/ClaimsLookUp/?userID=' + userId).success(function (response) {

        localStorageService.set('userPageClaims',response);
        $rootScope.$broadcast('loginStatusChanged', null);
        deferred.resolve(response);
      }).error(function (err, status) {
        factory.logOut();
        deferred.reject(err);
      });

      return deferred.promise;
    };

    factory.getUserRoles = function (userId) {
      var deferred = $q.defer();
      $http.get(serviceBase +  "odata/Users('" + userId + "')/Roles").success(function (response) {
        localStorageService.set('userRoles',response.value);
        deferred.resolve(response);
      }).error(function (err, status) {
        factory.logOut();
        deferred.reject(err);
      });

      return deferred.promise;
    };

    factory.fillAuthData = function () {
      var authData = localStorageService.get('authorizationData');
      if (authData) {
        factory.authentication.isAuth = true;
        factory.authentication.userName = authData.userName;
      }
    };

    factory.redirectToLogin = function () {
      $rootScope.$broadcast('redirectToLogin', null);
    };
    return factory;
  };

  authFactory.$inject = injectParams;

  app.factory('authService', authFactory);

});
