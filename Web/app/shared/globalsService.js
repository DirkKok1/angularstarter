'use strict';

define(['app'], function (app) {

  var injectParams = ['localStorageService'];

  var globalsFactory = function (localStorageService) {

    var factory = {};

    factory.urls = {
      api: 'http://localhost:38725/'
    };

    factory.url = function (which) {
      return factory.urls[which];
    };

    factory.getTokenHeader = function () {
      return 'Authorization', 'Bearer ' + localStorageService.get('authorizationData').token;
    };

    factory.dateFormat = "DD/MM/YYYY";
    factory.dtoDateFormat = "MM/DD/YYYY";

    factory.generateUUID = function() {
      var d = new Date().getTime();
      var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
          var r = (d + Math.random()*16)%16 | 0;
          d = Math.floor(d/16);
          return (c=='x' ? r : (r&0x3|0x8)).toString(16);
      });
      return uuid;
    };

    factory.generateRandomColour = function()
    {
      var letters = '0123456789ABCDEF'.split('');
      var color = '#';

      for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
      }

      return color;
    };

    factory.formIsValid = function(form){
        if (!form)
        {
          return false;
        }
        return form.$valid;
    };

    factory.find = function(array, property, value) {
        if (property)
          return Enumerable.From(array).FirstOrDefault(undefined, function(x) { return x[property] == value; });
       else
         return Enumerable.From(array).FirstOrDefault(undefined, function(x) { return x == value; });
    };

    factory.indexOf = function(array, property, value) {
      return Enumerable.From(array).FirstOrDefault(undefined, function(x, i) { if(x[property] == value) { return i}; });
    };

    return factory;
  };

  globalsFactory.$inject = injectParams;

  app.factory('globalsService', globalsFactory);

});
