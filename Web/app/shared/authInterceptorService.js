'use strict';

define(['app'], function (app) {

  var injectParams = ['$q', '$location','$injector', 'localStorageService'];

  var authInterceptorServiceFactory = function ($q, $location,$injector, localStorageService) {

    var factory = {};

    factory.request = function (config) {

      config.headers = config.headers || {};

      var authData = localStorageService.get('authorizationData');
      if (authData) {
        config.headers['Authorization'] = 'Bearer ' + authData.token;
      }
      return config;

    };

    factory.responseError = function (rejection) {
      var deferred = $q.defer();
      if (rejection.status === 401) {
        var authService = $injector.get('authService');
        authService.refreshToken().then(function (response) {
          factory.retryHttpRequest(rejection.config, deferred);
        }, function () {
          authService.logOut();
          $location.path('/login');
          deferred.reject(rejection);
        });
      } else {
        deferred.reject(rejection);
      }
      return deferred.promise;
    }

    factory.retryHttpRequest = function (config, deferred) {
      $http =  $injector.get('$http');
      $http(config).then(function (response) {
        deferred.resolve(response);
      }, function (response) {
        deferred.reject(response);
      });
    }

    return factory;
  };

  authInterceptorServiceFactory.$inject = injectParams;

  app.factory('authInterceptorService', authInterceptorServiceFactory);

});
