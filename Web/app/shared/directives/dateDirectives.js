'use strict';

var isValidDateRange = function (fromDate, toDate) {
  if (!fromDate.isValid()){
    return false;
  }
  if (!toDate.isValid()){
    return false;
  }

  return fromDate.isBefore(toDate, 'day') || fromDate.isSame(toDate, 'day');
};

define(['app'], function (app) {
  app.directive('dateLowerThan', ["$filter", function ($filter) {
    return {
      require: 'ngModel',
      link: function (scope, elm, attrs, ctrl) {
        var validateDateRange = function (inputValue) {
          var fromDate = moment(inputValue, attrs.dateLowerThanFormat);
          var toDate = moment(attrs.dateLowerThan, attrs.dateLowerThanFormat);
          var isValid = isValidDateRange(fromDate, toDate);
          ctrl.$setValidity('dateLowerThan', isValid);
          return inputValue;
        };

        ctrl.$parsers.unshift(validateDateRange);
        ctrl.$formatters.push(validateDateRange);
        attrs.$observe('dateLowerThan', function () {
          validateDateRange(ctrl.$viewValue);
        });
      }
    };
  }])

  .directive('dateGreaterThan', ["$filter", function ($filter) {
    return {
      require: 'ngModel',
      link: function (scope, elm, attrs, ctrl) {
        var validateDateRange = function (inputValue) {
          var fromDate = moment(attrs.dateGreaterThan, attrs.dateGreaterThanFormat);
          var toDate = moment(inputValue, attrs.dateGreaterThanFormat);
          var isValid = isValidDateRange(fromDate, toDate);
          ctrl.$setValidity('dateGreaterThan', isValid);
          return inputValue;
        };

        ctrl.$parsers.unshift(validateDateRange);
        ctrl.$formatters.push(validateDateRange);
        attrs.$observe('dateGreaterThan', function () {
          validateDateRange(ctrl.$viewValue);
        });
      }
    };
  }]);
});
