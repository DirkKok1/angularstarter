'use strict';

define(['app'], function (app) {
  app.directive('passwordCharactersValidator', function () {

    var REQUIRED_PATTERNS = [
      /\d+/,    //numeric values
      /[a-z]+/, //lowercase values
      /[A-Z]+/, //uppercase values
      /\W+/,    //special characters
      /^\S+$/   //no whitespace allowed
    ];

    return {
      require: 'ngModel',
      link: function ($scope, element, attrs, ngModel) {
        ngModel.$validators.passwordCharacters = function (value) {
          var status = true;
          angular.forEach(REQUIRED_PATTERNS, function (pattern) {
            status = status && pattern.test(value);
          });
          return status;
        };
      }
    };
  })

  .directive('checkUnique', function ($parse) {
    return {
      require: 'ngModel',
      restrict: 'A',
      scope: { method:'&checkUnique' },
      link: function (scope, element, attrs, ngModel) {
        scope.$watch(attrs.checkUnique, function () {
          ngModel.$validate();
        });

        var expressionHandler = scope.method();

        ngModel.$validators.checkUnique = function (value) {
          var result = expressionHandler(value);
          return !result;
        };
      }
    };
  })

  .directive('compareToValidator', function () {
    return {
      require: 'ngModel',
      link: function (scope, element, attrs, ngModel) {
        scope.$watch(attrs.compareToValidator, function () {
          ngModel.$validate();
        });
        ngModel.$validators.compareTo = function (value) {
          var other = scope.$eval(attrs.compareToValidator);
          return !value || !other || value == other.$viewValue;
        };
      }
    };
  });
});
