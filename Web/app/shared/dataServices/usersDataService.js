'use strict';

define(['app'], function (app) {

  var injectParams = ['$http', 'globalsService'];

  var usersDataService = function ($http, globalsService) {

    var factory = {};

    factory.serviceBaseUrl = globalsService.url('api') + 'odata/Users';

    factory.addUser = function(user) {
      return $http.post(factory.serviceBaseUrl, user);
    };

    factory.updateUser = function (user) {
      return $http.put(factory.serviceBaseUrl + "('" + user.Id + "')", user);
    };

    factory.getUserById = function (id) {
      return $http.get(factory.serviceBaseUrl + "('" + id + "')?$expand=Roles");
    };

    factory.getUsers = function() {
      return $http.get(factory.serviceBaseUrl);
    };

    factory.deleteUserById = function (id) {
      return $http.delete(factory.serviceBaseUrl + "('" + id + "')");
    };

    factory.getRolesByUserId = function (id) {
      return $http.get(factory.serviceBaseUrl + "('" + id + "')/Roles");
    }

    return factory;
  };

  usersDataService.$inject = injectParams;

  app.factory('usersDataService', usersDataService);

});
