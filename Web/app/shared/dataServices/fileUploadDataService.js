'use strict';

define(['app'], function (app) {

  var injectParams = ['$http', 'globalsService'];

  var fileUploadDataService = function ($http, globalsService) {

    var factory = {};

    factory.getServiceUrl = function () {
      return globalsService.url('api') + "api/" + "FileUpload";
    };

    return factory;
  };

  fileUploadDataService.$inject = injectParams;

  app.factory('fileUploadDataService', fileUploadDataService);

});
