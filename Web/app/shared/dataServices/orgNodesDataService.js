'use strict';

define(['app'], function (app) {

  var injectParams = ['$http', 'globalsService'];

  var orgNodesDataService = function ($http, globalsService) {

    var factory = {};

    factory.serviceBaseUrl = globalsService.url('api') + 'odata/OrgNodes';

    factory.addOrgNode = function(orgNode) {
      return $http.post(factory.serviceBaseUrl, orgNode);
    };

    factory.updateOrgNode = function (orgNode) {
      return $http.put(factory.serviceBaseUrl + "(" + orgNode.Id + ")", orgNode);
    };

    factory.getOrgNodeById = function (id) {
      return $http.get(factory.serviceBaseUrl + "(" + id + ")");
    };

    factory.getOrgNodes = function() {
      return $http.get(factory.serviceBaseUrl);
    };

   factory.deleteOrgNodeById = function (id) {
      return $http.delete(factory.serviceBaseUrl + "(" + id + ")");
    };

    return factory;
  };

  orgNodesDataService.$inject = injectParams;

  app.factory('orgNodesDataService', orgNodesDataService);

});
