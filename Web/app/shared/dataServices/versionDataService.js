'use strict';

define(['app'], function (app) {

  var injectParams = ['$http', 'globalsService'];

  var versionDataService = function ($http, globalsService) {

    var factory = {};

    factory.serviceBaseUrl = globalsService.url('api') + 'api/Version';

    factory.getVersion = function() {
      return $http.get(factory.serviceBaseUrl);
    };

    return factory;
  };

  versionDataService.$inject = injectParams;

  app.factory('versionDataService', versionDataService);

});
