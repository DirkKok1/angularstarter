'use strict';

define(['app'], function (app) {

  var injectParams = ['$http', 'globalsService'];

  var ordersDataService = function ($http, globalsService) {

    var factory = {};

    factory.serviceBaseUrl = globalsService.url('api') + 'odata/Orders';

    factory.addOrder = function(order) {
      return $http.post(factory.serviceBaseUrl, order);
    };

    factory.updateOrder = function (order) {
      return $http.put(factory.serviceBaseUrl + "(" + order.Id + ")", order);
    };

    factory.getOrderById = function (id) {
      return $http.get(factory.serviceBaseUrl + "(" + id + ")?$expand=OrderLines");
    };

    factory.getOrders = function() {
      return $http.get(factory.serviceBaseUrl);
    };

    factory.deleteOrderById = function (id) {
      return $http.delete(factory.serviceBaseUrl + "(" + id + ")");
    };

    factory.getOrderLinesByOrderId = function (id) {
      return $http.get(factory.serviceBaseUrl + "(" + id + ")/OrderLines");
    }

    return factory;
  };

  ordersDataService.$inject = injectParams;

  app.factory('ordersDataService', ordersDataService);

});
