'use strict';

define(['app'], function (app) {

  var injectParams = ['$http', 'globalsService'];

  var rolesDataService = function ($http, globalsService) {

    var factory = {};

    factory.serviceBaseUrl = globalsService.url('api') + 'odata/Roles';

    factory.addRole = function(role) {
      return $http.post(factory.serviceBaseUrl, role);
    };

    factory.updateRole = function (role) {
      return $http.put(factory.serviceBaseUrl + "('" + role.Id + "')", role);
    };

    factory.getRoleById = function (id) {
      return $http.get(factory.serviceBaseUrl + "('" + id + "')?$expand=Claims");
    };

    factory.getRoles = function() {
      return $http.get(factory.serviceBaseUrl);
    };

    factory.deleteRoleById = function (id) {
      return $http.delete(factory.serviceBaseUrl + "('" + id + "')");
    };

    factory.getClaimsByRoleId = function (id) {
      return $http.get(factory.serviceBaseUrl + "('" + id + "')/Claims");
    }

    return factory;
  };

  rolesDataService.$inject = injectParams;

  app.factory('rolesDataService', rolesDataService);

});
