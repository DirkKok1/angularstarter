'use strict';

define(['app'], function (app) {

  var injectParams = ['$http', 'globalsService'];

  var claimsDataService = function ($http, globalsService) {

    var factory = {};

    factory.serviceBaseUrl = globalsService.url('api') + 'odata/Claims';

    factory.getClaims = function() {
      return $http.get(factory.serviceBaseUrl);
    };

    return factory;
  };

  claimsDataService.$inject = injectParams;

  app.factory('claimsDataService', claimsDataService);

});
