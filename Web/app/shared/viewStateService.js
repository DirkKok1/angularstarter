'use strict';

define(['app'], function (app) {

  var injectParams = ['localStorageService'];

  var viewStateService = function (localStorageService) {

    var factory = {};

    factory.orgNodeExpandedStates = getOrgNodeExpandedData();
    factory.selectedOrgNodeId = localStorageService.get('orgNodeSelectedData');

    factory.clear = function () {
      localStorageService.set('orgNodeSelectedData', null);
      localStorageService.set('orgNodeExpandedData', null);

      factory.orgNodeExpandedStates = getOrgNodeExpandedData();
      factory.selectedOrgNodeId = localStorageService.get('orgNodeSelectedData');
    }

    function getOrgNodeExpandedData() {
      var orgNodeExpandedData = localStorageService.get('orgNodeExpandedData');
      if (!orgNodeExpandedData) {
        orgNodeExpandedData = {};
      }
      return orgNodeExpandedData;
    }

    factory.storeOrgNodeSelectedState = function (id) {
      factory.selectedOrgNodeId = id;
      localStorageService.set('orgNodeSelectedData', factory.selectedOrgNodeId);
    };

    factory.storeOrgNodeExpandedState = function (id, expanded) {

      if (expanded) {
        factory.orgNodeExpandedStates[id] = true;
      }
      else {
        delete factory.orgNodeExpandedStates[id];
      }

      localStorageService.set('orgNodeExpandedData', factory.orgNodeExpandedStates);
    };

   

    return factory;
  };

  viewStateService.$inject = injectParams;

  app.factory('viewStateService', viewStateService);

});
