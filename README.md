# README #

### What is this repository for? ###

* Quick summary

This project is a starter project for an AngularJS + WebAPI + EntityFramework LOB application

It uses HTML 5 Bootstrap and Telerik Kendo components

* Features include:

1. Token based security
2. Role Management
3. User Management

### How do I get set up? ###

* Summary of set up

1. Open a command prompt and run "npm install -g grunt-cli bower yo generator-karma generator-angular"
2. Open the visual studio solution
3. Update the web.config and change the database connection strings to point to your local server. No need to create database.
4. In package manager console run: update-database (this will create the db and populate with seed data.)
5. Open command window in the web folder
6. run NPM install
7. run Bower install
8. grunt serve
9. log in with username: admin, password: password